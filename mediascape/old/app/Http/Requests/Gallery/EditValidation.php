<?php

namespace App\Http\Requests\Gallery;

use Illuminate\Foundation\Http\FormRequest;

class EditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'              =>  'required',
            'image'              =>  'image',
        ];
    }

    public function messages() {
        return [
            'title.required'                => 'Please, Add Name.',
            'title.min'                     => 'Please, Add min 8 characters.',
            'image.required'                => 'Please, Add image.',
            'image.image'                     => 'Please, Add image type.',
            'image.mimes'                => 'Please, Add image type in jpg/png.',
        ];
    }
}
