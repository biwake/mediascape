<?php

namespace App\Http\Requests\Page;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class Add extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:page',
            'slug' => 'required|unique:page',
            'status' => 'required|accepted:1,0',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Title Field Is Required',
            'dropdown.required' => 'dropdown is not required',
            'dropdown.accepted' => 'dropdown is not valited',
            'target.required' => 'target is not required',
            'target.accepted' => 'target is not valited',
            'status.required' => 'status is not required',
            'status.accepted' => 'status is not valited',
        ];
    }

}
