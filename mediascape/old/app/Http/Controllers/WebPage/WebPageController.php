<?php

namespace App\Http\Controllers\WebPage;

use App\Models\Menu;
use App\Models\Order;
use App\Models\AboutUs;
use App\Models\Banner;
use App\Models\Feature;
use App\Models\Gallery;
use App\Models\News;
use App\Models\Page;
use App\Models\Pakage;
use App\Models\Principle;
use App\Models\Profile;
use App\Models\Section;
use App\Models\Services;
use Illuminate\Http\Request;

class WebPageController extends BasicWebPageController
{

    protected $view_path = 'frontend.home.';

    public function index(){

        $data = [];
        $data['banner'] = Banner::select('id', 'title', 'image', 'description','status')
            ->orderBy('id')
            ->limit(4)
            ->where('status', 1)
            ->get();
        $section_id = Section::select('id')->where('slug','=','section-nav-bar')->first();
        $data['second_menu'] = Menu::select()->where('section_id','=',$section_id->id)->orderBy('order_by')->get();
        $data['profile'] = Profile::first();
        $data['principle'] = Principle::select('id','title', 'image', 'message','status')->first();
        $data['news'] = News::select('title', 'slug', 'image', 'short_description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->limit(3)
            ->get();
        $data['feature'] = Feature::select('title', 'slug', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->limit(5)
            ->get();
        $data['pakage'] = Pakage::select('id','title')->where('status',1)->get();
        $data['services'] = Services::select('title', 'id', 'image', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->get();
        $data['gallery'] = Gallery::select('id', 'title', 'image', 'status')
            ->limit(6)
            ->get();
        $data['about'] = AboutUs::select('id','title', "description", 'status')
            ->where('status', 1)
            ->first();

        return view(Parent::loadDefaultVars($this->view_path.'index'), compact('data'));

    }

    public function about()
    {
        $data['about'] = AboutUs::select('id','title', 'description', "image", 'status')
            ->where('status', 1)
            ->get();
        $data['profile'] = Profile::first();
        $data['services'] = Services::select('title', 'id', 'image', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->get();

        return view(Parent::loadDefaultVars('frontend.home.about'), compact('data'));
    }

    public function contact()
    {
        $data = [];
        $data['profile'] = Profile::first();
        $data['services'] = Services::select('title', 'id', 'image', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->get();
        return view(Parent::loadDefaultVars('frontend.home.contact'), compact('data'));
    }

    public function portflio()
    {
        $data['gallery'] = Gallery::select('id', 'title', 'image', 'status')
            ->paginate(10);
        $data['profile'] = Profile::first();
        $data['services'] = Services::select('title', 'id', 'image', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->get();
        return view(Parent::loadDefaultVars('frontend.home.portflio'), compact('data'));
    }

    public function action($slug=null)
    {
        $data = [];
        $data['profile'] = Profile::first();
        $data['services'] = Services::select('title', 'id', 'image', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->get();
        $data['service'] = Services::all()->first();
        return view(Parent::loadDefaultVars('frontend.home.services'), compact('data'));
    }

    public function onlineorder(Request $request)
    {
        $set = Order::create([
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'pakage_id' => $request->get('pakege'),
            'message' => $request->get('message'),
            'status' => 0,
        ]);

        $request->session()->flash('message', 'Online order successfully. Now check your email.');
        return redirect()->route('frontend.home');
    }

    public function ourteam($slug=null)
    {

        $a = isset($slug)?$slug:'creative-department';

        $data = [];
        $data['ourteam'] = Page::select()->where('slug','=',$a)->first();
        $parent_id = Section::select('id')->where('slug','=','our-team-sidebar')->first();
        $data['sidebar'] = Menu::select()->where('section_id','=',$parent_id->id)->get();

        return view(Parent::loadDefaultVars('frontend.home.ourteam'), compact('data'));
    }

}
