<?php

namespace App\Http\Controllers\WebPage;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Profile;
use App\Models\Section;
use View;

class BasicWebPageController extends Controller
{
    protected function loadDefaultVars($path)
    {

        View::composer($path, function($view){
            $section_id = Section::select('id')->where('slug','=','top-nav-bar')->first();
            $view->with('menu', Menu::select()->where('section_id','=',$section_id->id)->orderBy('order_by')->get());

            $section_id = Section::select('id')->where('slug','=','sidebar')->first();
            $view->with('sidebar', Menu::select()->where('section_id','=',$section_id->id)->get());

            $view->with('profile', Profile::select()->first());
        });

        return $path;

    }


}
