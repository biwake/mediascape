<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Profile\EditValidation;
use App\Models\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class ProfileController extends AdminBaseController
{
    protected $base_route = 'admin.profile';
    protected $view_path = 'admin.profile';
    protected $view_title = 'Profile Manger';
    protected $folder_name = 'profile';
    protected $trans_path = 'profile';
    protected $listing;
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index()
    {
        return view(parent::loadDefaultVars($this->view_path.'.profile'), compact('data'));
    }

    public function updatePassword(Request $request,$id)
    {

        $old_database_password = Auth::user()->password;
        if (Hash::check($request->get('old-password'), $old_database_password))
        {
            $new_password = ($request->get('new-password'));
            $new_re_password = ($request->get('re-new-password'));

            if ($new_password==$new_re_password){
                if (!$listing = User::find($id)){

                }else{
                    $listing->update([
                        'password' => bcrypt($new_password)
                    ]);

                    return redirect()->back()->with('message','Password Changed successfully');
                }
            }else{
                return redirect()->back()->with('error-message','New Password not match');
            }
        }else{
            return redirect()->back()->with('error-message','Old Password not match');
        }

    }

    public function edit(Request $request)
    {
        // get user data for $id
        //dd($id);
        $data = [];
        $data['row'] = Profile::select()->first();

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }else{
            $file_name = $request->get('oldimg');
        }

        if (!$listing = Profile::find($id)){
            $lii=Profile::create([
                'title' => $request->get('title'),
                'image' => $file_name,
                'address' => $request->get('address'),
                'phone' => $request->get('phone'),
                'mobile' => $request->get('mobile'),
                'email' => $request->get('email'),
                'facebook' => $request->get('facebook'),
                'twitter' => $request->get('twitter'),
                'youtube' => $request->get('youtube'),
            ]);
        }else{
            $listing->update([
                'title' => $request->get('title'),
                'image' => $file_name,
                'address' => $request->get('address'),
                'phone' => $request->get('phone'),
                'mobile' => $request->get('mobile'),
                'email' => $request->get('email'),
                'facebook' => $request->get('facebook'),
                'twitter' => $request->get('twitter'),
                'youtube' => $request->get('youtube'),
            ]);
        }






        $request->session()->flash('message', 'Profile updated successfully.');
        return redirect()->route($this->base_route);
    }



}