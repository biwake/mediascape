<?php

namespace App\Http\Controllers\Admin;

use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends AdminBaseController
{

    public $base_route = 'admin.feedback';
    public $view_path = 'admin/feedback';
    public $view_title = 'Feedback Manager';
    public $trans_path = 'santosh';

    public function index()
    {
        $data = [];
        $data['rows'] = Feedback::select()->paginate(10);

        return view(parent::loadDefaultVars($this->view_path.'.list'),compact('data'));
    }

    public function update(Request $request,$id)
    {

        if (!$data = Feedback::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $data->update([
            'name' => $data['name'],
            'email' => $data['email'],
            'message' => $data['message'],
            'status' => ($data['status']==0)?'1':'0',
        ]);

        return redirect()->route($this->base_route.'.list');

    }

}
