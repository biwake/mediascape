<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Feature\AddValidation;
use App\Http\Requests\Feature\EditValidation;
use App\Models\Feature;
use Illuminate\Http\Request;


class FeatureController extends AdminBaseController
{
    protected $base_route = 'admin.feature';
    protected $view_path = 'admin.feature';
    protected $view_title = 'Feature Manger';
    protected $trans_path = 'Feature Manger';


    public function index(Request $request)
    {

        $data = [];
        $data['rows'] = Feature::select('id','title', 'description','status')
            ->orderBy('id')
            ->paginate(30);

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddValidation $request)
    {

       $list = Feature::create([
           'title'               =>     $request->get('title'),
           'slug'               =>     str_slug($request->get('title')),
           'description'          =>     $request->get('description'),
           'status' => $request->get('status'),
        ]);

        $request->session()->flash('message', 'Feature added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        //dd($id);
        $data = [];

        if (!$data['santosh'] = Feature::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['row'] = Feature::find($id);

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }else{
            $file_name = $request->get('oldimg');
        }

        if (!$listing = Feature::find($id))
            return redirect()->route('admin.error', ['code' => '500']);


        $listing->update([
            'title'               =>     $request->get('title'),
            'slug'               =>     str_slug($request->get('title')),
            'description'          =>     $request->get('description'),
            'status' => $request->get('status'),
        ]);




        $request->session()->flash('message', 'Feature updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
       //dd('enter');
        if (!$listing = Feature::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        $request->session()->flash('message', 'Feature deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}