<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Services\AddValidation;
use App\Http\Requests\Services\EditValidation;
use App\Models\Services;
use Illuminate\Http\Request;


class ServicesController extends AdminBaseController
{
    protected $base_route = 'admin.services';
    protected $view_path = 'admin.services';
    protected $view_title = 'Services Manger';
    protected $folder_name = 'services';
    protected $trans_path = 'services';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }


    public function index(Request $request)
    {
        if ($request->has('name')) {
            $data = [];
            $data['rows'] = Services::select('id', 'title', 'image', 'description','status')
                ->where('title', 'LIKE', '%'.request()->get('name').'%')
                ->paginate(30);
        } else{
        $data = [];
        $data['rows'] = Services::select('id','title', 'image', 'description','status')
            ->orderBy('id')
            ->paginate(30);
        }

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddValidation $request)
    {

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }

       $list = Services::create([
           'title'               =>     $request->get('title'),
           'image'                =>    $file_name,
           'description'          =>     $request->get('description'),
           'status' => $request->get('status'),
        ]);

        $request->session()->flash('message', 'Services added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        //dd($id);
        $data = [];

        if (!$data['santosh'] = Services::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['row'] = Services::find($id);

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }else{
            $file_name = $request->get('oldimg');
        }

        if (!$listing = Services::find($id))
            return redirect()->route('admin.error', ['code' => '500']);


        $listing->update([
            'title'               =>     $request->get('title'),
            'image'                =>    $file_name,
            'description'          =>     $request->get('description'),
            'status' => $request->get('status'),
        ]);




        $request->session()->flash('message', 'Services updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
       //dd('enter');
        if (!$listing = Services::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        $request->session()->flash('message', 'Services deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}