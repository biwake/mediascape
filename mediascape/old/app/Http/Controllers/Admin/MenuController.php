<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Menu\Add;
use App\Models\Menu;
use App\Models\Page;
use App\Models\Section;
use Illuminate\Http\Request;

class MenuController extends AdminBaseController
{

    public $base_route = 'admin.menu';
    public $view_path = 'admin/menu';
    public $view_title = 'Menu Manager';
    public $trans_path = 'khusi';

    public function index(Request $request)
    {
        $data = [];


            /*$data['parent'] = Menu::select()->where('parent_id','=',0)->where('section_id',5)->paginate(1000);*/
            $data['child'] = Section::select()->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'),compact('data'));
    }

    public function menu(Request $request, $slug)
    {
        $section = Section::select('id')->where('slug',$slug)->first();

        $data = [];

            $data['child'] = Menu::select()->where('section_id',$section->id)->where('parent_id','=',0)->paginate(1000);

        return view(parent::loadDefaultVars($this->view_path.'.minu'),compact('data'));
    }

    public function menuSubmenu(Request $request, $slug)
    {
        $section = Menu::select('id')->where('slug',$slug)->first();

        $data = [];

            $data['child'] = Menu::select()->where('parent_id',$section->id)->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'),compact('data'));
    }

    public function add()
    {
        $section = Section::select('id')->where('slug','top-nav-bar')->first();

        $data = [];
        $data['menu'] = Menu::select('title','id')->where('section_id',$section->id)->where('parent_id','=',0)->get();
        $data['section'] = Section::select('title','id')->get();
        $data['page'] = Page::select('title','id')->get();

        if (isset($data['section']) && $data['section']->count()<1)
            return redirect()->route('admin.section.add');

        else if (isset($data['page']) && $data['page']->count()<1)
            return redirect()->route('admin.page.add');

        else
            return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(Add $request)
    {

        if ($request->get('dropdown') == 0)
            $parent_id = 0;

        else
            $parent_id = $request->get('parent_id');

        Menu::create([
            'title' => $request->get('title'),
            'slug' => str_slug($request->get('slug')),
            'link' => $request->get('link'),
            'parent_id' => $parent_id,
            'page_id' => $request->get('page_id'),
            'section_id' => $request->get('section_id'),
            'target' => $request->get('target'),
            'order_by' => $request->get('order_by'),
            'status' => $request->get('status'),
        ]);
        return redirect()->route($this->base_route.'.index');

    }

    public function edit($id)
    {

        $data = [];
        $data['row'] = Menu::find($id);
        $data['menu'] = Menu::select('title','id')->get();
        $data['section'] = Section::select('title','id')->get();
        $data['page'] = Page::select('title','id')->get();

        if (isset($data['section']) && $data['section']->count()<1)
            return redirect()->route('admin.section.add');

        else if (isset($data['page']) && $data['page']->count()<1)
            return redirect()->route('admin.page.add');

        else
            return View(parent::loadDefaultVars($this->view_path.'.edit'),compact('data'));

    }

    public function update(Request $request,$id)
    {

        if (!$data = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        if ($request->get('dropdown') == 0)
            $parent_id = 0;

        else
            $parent_id = $request->get('parent_id');

        $data->update([
            'title' => $request->get('title'),
            'link' => $request->get('link'),
            'parent_id' => $parent_id,
            'page_id' => $request->get('page_id'),
            'section_id' => $request->get('section_id'),
            'target' => $request->get('target'),
            'order_by' => $request->get('order_by'),
            'status' => $request->get('status'),
        ]);

        return redirect()->route($this->base_route.'.index');

    }

    public function delete(Request $request, $id)
    {
        //dd('enter');
        if (!$listing = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        $request->session()->flash('message', 'Gallery deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function search(Request $request)
    {
        $data = [];

        $data['error'] = true;



        $data['child'] = Menu::select()->where('parent_id','!=',0)->paginate(1000);
        $data['parent'] = Menu::select()->where('parent_id','=',0)->where('title', 'LIKE', '%'.$request->get('value').'%')->paginate(1000);

        $data['html'] = $this->view_path.'.list';

        $data['error'] = false;

        return response()->json(json_encode($data));

    }

}
