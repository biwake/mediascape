<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Section\Add;
use App\Models\Section;
use Illuminate\Http\Request;

class SectionController extends AdminBaseController
{

    public $base_route = 'admin.section';
    public $view_path = 'admin/section';
    public $view_title = 'Section Manager';
    public $trans_path = 'santosh';

    public function index()
    {
        $data = [];
        $data['rows'] = Section::select()->paginate(10);

        return view(parent::loadDefaultVars($this->view_path.'.list'),compact('data'));
    }

    public function add()
    {

        return view(parent::loadDefaultVars($this->view_path.'.add'));
        
    }

    public function store(Add $request)
    {

        Section::create([
            'title' => $request->get('title'),
            'slug' => str_slug($request->get('slug')),
            'description' => $request->get('description'),
            'status' => $request->get('status'),
        ]);
        return redirect()->route($this->base_route.'.index');

    }

    public function edit($id)
    {

        $data = [];
        $data['row'] = Section::find($id);

        return View(parent::loadDefaultVars($this->view_path.'.edit'),compact('data'));

    }

    public function update(Request $request,$id)
    {

        if (!$data = Section::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $data->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'status' => $request->get('status'),
        ]);

        return redirect()->route($this->base_route.'.index');

    }

    public function delete(Request $request, $id)
    {
        //dd('enter');
        if (!$listing = Section::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        $request->session()->flash('message', 'Gallery deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }

}
