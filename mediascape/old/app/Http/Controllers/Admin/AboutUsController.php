<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\AboutUs\AddValidation;
use App\Http\Requests\AboutUs\EditValidation;
use App\Models\AboutUs;
use Illuminate\Http\Request;


class AboutUsController extends AdminBaseController
{
    protected $base_route = 'admin.about-us';
    protected $view_path = 'admin.about-us';
    protected $view_title = 'AboutUs Manger';
    protected $folder_name = 'about-us';
    public $trans_path = 'santosh';
    protected $listing;
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }


    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = AboutUs::select('id','title', 'image', 'description', 'status')
            ->orderBy('id')
            ->paginate(30);

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddValidation $request)
    {

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }

       $list = AboutUs::create([
           'title'               =>     $request->get('title'),
           'slug' => str_slug($request->get('title')),
           'image'               =>     $file_name,
           'description'               =>     $request->get('description'),
           'status'          =>     $request->get('status'),
        ]);

        $request->session()->flash('message', 'AboutUs added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        //dd($id);
        $data = [];

        if (!$data['santosh'] = AboutUs::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['row'] = AboutUs::find($id);

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {
        if (!$listing = AboutUs::find($id))
            return redirect()->route('admin.error', ['code' => '500']);



        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }else{
            $file_name = $request->get('oldimg');
        }


        $listing->update([
            'title'               =>     $request->get('title'),
            'slug' => str_slug($request->get('title')),
            'image'               =>     $file_name,
            'description'               =>     $request->get('description'),
            'status'          =>     $request->get('status'),
        ]);


        $request->session()->flash('message', 'AboutUs updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
       //dd('enter');
        if (!$listing = AboutUs::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        $request->session()->flash('message', 'AboutUs deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}