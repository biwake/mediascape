<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Banner\AddValidation;
use App\Http\Requests\Banner\EditValidation;
use App\Models\Banner;
use Illuminate\Http\Request;


class BannerController extends AdminBaseController
{
    protected $base_route = 'admin.banner';
    protected $view_path = 'admin.banner';
    protected $view_title = 'Banner Manger';
    protected $folder_name = 'banner';
    protected $trans_path = 'banner';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }


    public function index(Request $request)
    {
        if ($request->has('name')) {
            $data = [];
            $data['rows'] = Banner::select('id', 'title', 'image', 'description','status')
                ->where('title', 'LIKE', '%'.request()->get('name').'%')
                ->paginate(30);
        } else{
        $data = [];
        $data['rows'] = Banner::select('id','title', 'image', 'description','status')
            ->orderBy('id')
            ->paginate(30);
        }

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddValidation $request)
    {

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }

       $list = Banner::create([
           'title'               =>     $request->get('title'),
           'image'                =>    $file_name,
           'description'          =>     $request->get('description'),
           'status' => $request->get('status'),
        ]);

        $request->session()->flash('message', 'Banner added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        //dd($id);
        $data = [];

        if (!$data['santosh'] = Banner::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['row'] = Banner::find($id);

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }else{
            $file_name = $request->get('oldimg');
        }

        if (!$listing = Banner::find($id))
            return redirect()->route('admin.error', ['code' => '500']);


        $listing->update([
            'title'               =>     $request->get('title'),
            'image'                =>    $file_name,
            'description'          =>     $request->get('description'),
            'status' => $request->get('status'),
        ]);




        $request->session()->flash('message', 'Banner updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
       //dd('enter');
        if (!$listing = Banner::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        $request->session()->flash('message', 'Banner deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function ajaxdelete(Request $request)
    {

        dd($request->all());
        
    }



}