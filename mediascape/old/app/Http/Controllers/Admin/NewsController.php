<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\News\AddValidation;
use App\Http\Requests\News\EditValidation;
use App\Models\News;
use Illuminate\Http\Request;


class NewsController extends AdminBaseController
{
    protected $base_route = 'admin.news';
    protected $view_path = 'admin.news';
    protected $view_title = 'News Manger';
    protected $folder_name = 'news';
    protected $trans_path = 'news';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }


    public function index(Request $request)
    {
        if ($request->has('name')) {
            $data = [];
            $data['rows'] = News::select('id', 'title', 'image', 'short-description', 'long-description', 'status')
                ->where('title', 'LIKE', '%'.request()->get('name').'%')
                ->paginate(30);
        } else{
        $data = [];
        $data['rows'] = News::select('id','title', 'image', 'short_description', 'long_description','status')
            ->orderBy('id')
            ->paginate(30);
        }

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddValidation $request)
    {


        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }

       $list = News::create([
           'title'               =>     $request->get('title'),
           'slug'               =>      str_slug($request->get('title')),
           'image'                =>    $file_name,
           'short_description'          =>     $request->get('short-description'),
           'long_description'          =>     $request->get('long-description'),
           'status' => $request->get('status'),
        ]);

        $request->session()->flash('message', 'News added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        //dd($id);
        $data = [];

        if (!$data['santosh'] = News::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['row'] = News::find($id);

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }else{
            $file_name = $request->get('oldimg');
        }

        if (!$listing = News::find($id))
            return redirect()->route('admin.error', ['code' => '500']);


        $listing->update([
            'title'               =>     $request->get('title'),
            'slug'               =>      str_slug($request->get('title')),
            'image'                =>    $file_name,
            'short_description'          =>     $request->get('short-description'),
            'long_description'          =>     $request->get('long-description'),
            'status' => $request->get('status'),
        ]);




        $request->session()->flash('message', 'News updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
       //dd('enter');
        if (!$listing = News::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        $request->session()->flash('message', 'News deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}