<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 11/7/2016
 * Time: 7:52 AM
 */

namespace App\HelperClasses;


use App\Models\Assign;
use App\Models\Gallery;
use App\User;

class ViewHelper
{
    public function getData($key, $data = [])
    {
        if (old($key))
            return old($key);
        elseif (count($data) > 0)
            return $data->$key;
        else
            return '';
    }

    public function galleryhelper($id,$counter)
    {
        $data['row'] = Gallery::select('id','title','image')->first();
        $data['counter'] = $counter;
        if ($counter<=2){
            echo view('frontend.home.gallery', ['data' => $data]);
        }else {
            echo view('webpage.home.bannerleft', ['data' => $data]);
        }
    }

}