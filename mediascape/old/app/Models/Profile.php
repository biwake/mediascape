<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['title', 'logo', 'address', 'image', 'phone', 'mobile', 'email', 'facebook', 'twitter', 'youtube'];




}
