<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

    protected $fillable = ['title', 'slug', 'link', 'parent_id', 'page_id', 'section_id', 'target', 'status', 'order_by'];


    public function parent()
    {
        return $this->belongsTo('App\Models\Menu', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Menu', 'parent_id');
    }

    public function page()
    {
        return $this->belongsTo('App\Models\Page','page_id');
    }

    public function section()
    {
        return $this->belongsTo('App\Models\Section','section_id');
    }

}
