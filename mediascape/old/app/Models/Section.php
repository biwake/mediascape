<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = 'section';

    protected $fillable = ['title', 'slug', 'description', 'status'];

    public function menus()
    {
        return $this->hasMany('App\Models\Menu','section_id');
    }

}
