<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pakage extends Model
{
    protected $table = 'pakages';

    protected $fillable = ['title', 'status'];
}
