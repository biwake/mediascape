<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'page';

    protected $fillable = ['title', 'slug', 'description', 'status'];

    public function menus()
    {
        return $this->hasMany('App\Models\Menu','page_id');
    }

}
