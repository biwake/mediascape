<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $table = 'feature';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['title', 'slug', 'description', 'status'];




}
