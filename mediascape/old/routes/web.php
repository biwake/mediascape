<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('',                    ['as' => 'frontend.home',             'uses' => 'WebPage\WebPageController@index']);
Route::get('home',                    ['as' => 'frontend.home',             'uses' => 'WebPage\WebPageController@index']);
Route::get('about-us',                    ['as' => 'frontend.about-us',             'uses' => 'WebPage\WebPageController@about']);
Route::get('contact-us',                    ['as' => 'frontend.contact-us',             'uses' => 'WebPage\WebPageController@contact']);
Route::get('portfolio',                    ['as' => 'frontend.portfolio',             'uses' => 'WebPage\WebPageController@portflio']);
Route::post('onlineorder',                    ['as' => 'frontend.onlineorder',             'uses' => 'WebPage\WebPageController@onlineorder']);
Route::get('services/',                    ['as' => 'frontend.services',             'uses' => 'WebPage\WebPageController@action']);
Route::get('services/{slug}',                    ['as' => 'frontend.service',             'uses' => 'WebPage\WebPageController@action']);
Route::get('our-team/{slug}',                    ['as' => 'frontend.ourteams',             'uses' => 'WebPage\WebPageController@ourteam']);
Route::get('our-team',                    ['as' => 'frontend.our-team',             'uses' => 'WebPage\WebPageController@ourteam']);

Auth::routes();

Route::get('logout', [ 'as' => 'logout', 'uses' => 'Auth\LoginController@logout' ]);

//Route::get('/', function () {
//    return redirect('login');
//});

/*Route::get('/home', function () {
    return redirect()->route('admin.listing.index');
});*/

Route::group(['middleware' => ['auth', 'status'], 'prefix' => '', 'as' => 'admin.', 'namespace' => 'Admin\\'], function () {

    Route::get('admin/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::get('admin/error/{code}',                        ['as' => 'error',                 'uses' => 'DashboardController@error']);

    Route::get('admin/profile/setting', ['as' => 'profile.setting', 'uses' => 'ProfileController@index']);
    Route::post('admin/profile/updatePassword/{id}', ['as' => 'profile.updatePassword', 'uses' => 'ProfileController@updatePassword']);

    Route::get('admin/profile', ['as' => 'profile', 'uses' => 'ProfileController@edit']);
    Route::post('admin/profile/{id}/update', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);

    Route::get('admin/feedback',['as' => 'feedback.list', 'uses' => 'FeedbackController@index']);
    Route::get('admin/feedback/{id}/update',          ['as' => 'feedback.update',          'uses' => 'FeedbackController@update']);

    Route::get('admin/about-us',                        ['as' => 'about-us.index',           'uses' => 'AboutUsController@index']);
    Route::get('admin/about-us/add',                    ['as' => 'about-us.add',             'uses' => 'AboutUsController@add']);
    Route::post('admin/about-us/store',                 ['as' => 'about-us.store',           'uses' => 'AboutUsController@store']);
    Route::get('admin/about-us/{id}/edit',              ['as' => 'about-us.edit',            'uses' => 'AboutUsController@edit']);
    Route::post('admin/about-us/{id}/update',          ['as' => 'about-us.update',          'uses' => 'AboutUsController@update']);
    Route::get('admin/about-us/{id}/delete',           ['as' => 'about-us.delete',          'uses' => 'AboutUsController@delete']);

    Route::get('admin/gallery',                        ['as' => 'gallery.index',           'uses' => 'GalleryController@index']);
    Route::get('admin/gallery/add',                    ['as' => 'gallery.add',             'uses' => 'GalleryController@add']);
    Route::post('admin/gallery/store',                 ['as' => 'gallery.store',           'uses' => 'GalleryController@store']);
    Route::get('admin/gallery/{id}/edit',              ['as' => 'gallery.edit',            'uses' => 'GalleryController@edit']);
    Route::post('admin/gallery/{id}/update',          ['as' => 'gallery.update',          'uses' => 'GalleryController@update']);
    Route::get('admin/gallery/{id}/delete',           ['as' => 'gallery.delete',          'uses' => 'GalleryController@delete']);

    Route::get('admin/banner',                        ['as' => 'banner.index',           'uses' => 'BannerController@index']);
    Route::get('admin/banner/add',                    ['as' => 'banner.add',             'uses' => 'BannerController@add']);
    Route::post('admin/banner/store',                 ['as' => 'banner.store',           'uses' => 'BannerController@store']);
    Route::get('admin/banner/{id}/edit',              ['as' => 'banner.edit',            'uses' => 'BannerController@edit']);
    Route::post('admin/banner/{id}/update',          ['as' => 'banner.update',          'uses' => 'BannerController@update']);
    Route::get('admin/banner/{id}/delete',           ['as' => 'banner.delete',          'uses' => 'BannerController@delete']);

    Route::get('admin/services',                        ['as' => 'services.index',           'uses' => 'ServicesController@index']);
    Route::get('admin/services/add',                    ['as' => 'services.add',             'uses' => 'ServicesController@add']);
    Route::post('admin/services/store',                 ['as' => 'services.store',           'uses' => 'ServicesController@store']);
    Route::get('admin/services/{id}/edit',              ['as' => 'services.edit',            'uses' => 'ServicesController@edit']);
    Route::post('admin/services/{id}/update',          ['as' => 'services.update',          'uses' => 'ServicesController@update']);
    Route::get('admin/services/{id}/delete',           ['as' => 'services.delete',          'uses' => 'ServicesController@delete']);

    Route::get('admin/contact',                        ['as' => 'contact.index',           'uses' => 'ContactController@index']);
    Route::get('admin/contact/add',                    ['as' => 'contact.add',             'uses' => 'ContactController@add']);
    Route::post('admin/contact/store',                 ['as' => 'contact.store',           'uses' => 'ContactController@store']);
    Route::get('admin/contact/{id}/edit',              ['as' => 'contact.edit',            'uses' => 'ContactController@edit']);
    Route::post('admin/contact/{id}/update',          ['as' => 'contact.update',          'uses' => 'ContactController@update']);
    Route::get('admin/contact/{id}/delete',           ['as' => 'contact.delete',          'uses' => 'ContactController@delete']);

    Route::get('admin/pakage',                        ['as' => 'pakage.index',           'uses' => 'PakageController@index']);
    Route::get('admin/pakage/add',                    ['as' => 'pakage.add',             'uses' => 'PakageController@add']);
    Route::post('admin/pakage/store',                 ['as' => 'pakage.store',           'uses' => 'PakageController@store']);
    Route::get('admin/pakage/{id}/edit',              ['as' => 'pakage.edit',            'uses' => 'PakageController@edit']);
    Route::post('admin/pakage/{id}/update',          ['as' => 'pakage.update',          'uses' => 'PakageController@update']);
    Route::get('admin/pakage/{id}/delete',           ['as' => 'pakage.delete',          'uses' => 'PakageController@delete']);

    Route::get('admin/onlineorder/{id}/reply',                    ['as' => 'onlineorder.reply',             'uses' => 'OnlineorderController@reply']);
    Route::post('admin/onlineorder/{id}/mail',                 ['as' => 'onlineorder.mail',           'uses' => 'OnlineorderController@mail']);
    Route::get('admin/onlineorder/{id}/edit',              ['as' => 'onlineorder.edit',            'uses' => 'OnlineorderController@edit']);
    Route::get('admin/onlineorder',                        ['as' => 'onlineorder.index',           'uses' => 'OnlineorderController@index']);
    Route::get('admin/onlineorder/{id}/update', ['as' => 'onlineorder.update', 'uses' => 'OnlineorderController@update']);
    Route::get('admin/onlineorder/{id}/delete',           ['as' => 'onlineorder.delete',          'uses' => 'OnlineorderController@delete']);

    Route::get('admin/menu',                        ['as' => 'menu.index',           'uses' => 'MenuController@index']);
    Route::post('admin/menu/search',                        ['as' => 'menu.search',           'uses' => 'MenuController@search']);
    Route::get('admin/menu/add',                    ['as' => 'menu.add',             'uses' => 'MenuController@add']);
    Route::post('admin/menu/store',                 ['as' => 'menu.store',           'uses' => 'MenuController@store']);
    Route::get('admin/menu/{id}/edit',              ['as' => 'menu.edit',            'uses' => 'MenuController@edit']);
    Route::post('admin/menu/{id}/update',          ['as' => 'menu.update',          'uses' => 'MenuController@update']);
    Route::get('admin/menu/{id}/delete',           ['as' => 'menu.delete',          'uses' => 'MenuController@delete']);
    Route::get('admin/menu/{slug}',           ['as' => 'admin.menu',          'uses' => 'MenuController@menu']);
    Route::get('admin/menu/submenu/{slug}',           ['as' => 'admin.menu.submenu',          'uses' => 'MenuController@menuSubmenu']);

    Route::get('admin/page',                        ['as' => 'page.index',           'uses' => 'PageController@index']);
    Route::get('admin/page/add',                    ['as' => 'page.add',             'uses' => 'PageController@add']);
    Route::post('admin/page/store',                 ['as' => 'page.store',           'uses' => 'PageController@store']);
    Route::get('admin/page/{id}/edit',              ['as' => 'page.edit',            'uses' => 'PageController@edit']);
    Route::post('admin/page/{id}/update',          ['as' => 'page.update',          'uses' => 'PageController@update']);
    Route::get('admin/page/{id}/delete',           ['as' => 'page.delete',          'uses' => 'PageController@delete']);

    Route::get('admin/section',                        ['as' => 'section.index',           'uses' => 'SectionController@index']);
    Route::get('admin/section/add',                    ['as' => 'section.add',             'uses' => 'SectionController@add']);
    Route::post('admin/section/store',                 ['as' => 'section.store',           'uses' => 'SectionController@store']);
    Route::get('admin/section/{id}/edit',              ['as' => 'section.edit',            'uses' => 'SectionController@edit']);
    Route::post('admin/section/{id}/update',          ['as' => 'section.update',          'uses' => 'SectionController@update']);
    Route::get('admin/section/{id}/delete',           ['as' => 'section.delete',          'uses' => 'SectionController@delete']);


});