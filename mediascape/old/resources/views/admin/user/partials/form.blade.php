<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

    <div class="col-sm-9">
        <input type="text" name="name" id="name" value="{{ ViewHelper::getData('name', isset($data['listing'])?$data['listing']:[]) }}" placeholder="Name" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Type </label>

    <div class="col-sm-9">
        <select name="type" id="type" class="col-xs-10 col-sm-5">
            <option value="mmanager" {{ isset($data['listing'])&&$data['listing']->type=='mmanager'?'selected':'' }}>Marketing Manager</option>
            <option value="marketing" {{ isset($data['listing'])&&$data['listing']->type=='marketing'?'selected':'' }}>Marketing Staff</option>
            <option value="msupport" {{ isset($data['listing'])&&$data['listing']->type=='msupport'?'selected':'' }}>Support Manager</option>
            <option value="support" {{ isset($data['listing'])&&$data['listing']->type=='support'?'selected':'' }}>Support</option>
            <option value="admin" {{ isset($data['listing'])&&$data['listing']->type=='admin'?'selected':'' }}>Admin</option>
            </select>
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address </label>

    <div class="col-sm-9">
        <input type="text" name="address" id="address" value="{{ ViewHelper::getData('address', isset($data['listing'])?$data['listing']:[]) }}" placeholder="Address" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>



<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Contact </label>

    <div class="col-sm-9">
        <input type="text" name="contact" id="contact" value="{{ ViewHelper::getData('contact', isset($data['listing'])?$data['listing']:[]) }}" placeholder="Contact" class="col-xs-10 col-sm-5">
    </div>
</div>

<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Image </label>

    <div class="col-sm-9">
        @if(isset($data['row']) && $data['row']->image!=='')
            <img src="{{ url('/images/user/').'/'.$data['row']->image }}" style="height: 100px; float: left;"><div style="clear: both"></div><br>
        @endif
        <input type="file" name="image" id="image" class="col-xs-10 col-sm-5">
    </div>
</div>

<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email</label>

    <div class="col-sm-9">
        <input type="text" name="email" id="email" value="{{ ViewHelper::getData('email', isset($data['listing'])?$data['listing']:[]) }}" placeholder="Email Address" class="col-xs-10 col-sm-5">
    </div>
</div>

<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password</label>

    <div class="col-sm-9">
        <input type="password" name="password" id="password" class="col-xs-10 col-sm-5">
    </div>
</div>

<div class="space-4"></div>

