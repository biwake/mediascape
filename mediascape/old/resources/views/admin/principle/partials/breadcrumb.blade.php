<ul class="breadcrumb">
    <li>
        <i class="icon-home home-icon"></i>
        <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
    </li>

    <li>
        <a href="{{ route($base_route.'.add') }}">{{ $view_title }} </a>
    </li>

    @if (isset($data))
        <li class="active">{{ trans($trans_path.'content.common.edit') }} </li>
    @else
        <li class="active">{{ trans($trans_path.'content.common.add') }} </li>
    @endif

</ul><!-- .breadcrumb -->