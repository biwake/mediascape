<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="title" id="title" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

@if(!isset($data['row']))
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="slug"> Slug </label>

        <div class="col-sm-9">
            <input type="text" name="slug" id="slug" value="{{ ViewHelper::getData('slug', isset($data['row'])?$data['row']:[]) }}" placeholder="Slug" class="col-xs-10 col-sm-5">
        </div>
    </div>
    <div class="space-4"></div>
@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="description"> Description </label>

    <div class="col-sm-9">
        <textarea name="description" id="description" placeholder="Description" class="col-xs-10 col-sm-5">{{ ViewHelper::getData('description', isset($data['row'])?$data['row']:[]) }}</textarea>
        <script>
            CKEDITOR.replace( 'description', {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
            });
        </script>
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status"> Status </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="status" value="1" {{ (isset($data['row'])&&$data['row']->status==1)?'checked':'' }} {{ isset($data['row'])?'':'checked' }} type="radio" class="ace">
                <span class="lbl"> Active</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="status" {{ (isset($data['row'])&&$data['row']->status==0)?'checked':'' }} value="0" type="radio" class="ace">
                <span class="lbl"> In-active</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>


<script>

    $(document).ready(function () {
        $('#title').keyup(function () {

            var title = $('#title').val();
            $('#slug').val(title.replace(/\s+/g, '-').toLowerCase());



        });
    });

</script>