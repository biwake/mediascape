<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9" class="col-xs-10 col-sm-5">
        <input type="text" name="title" id="title" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Status </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="status" value="1" {{ (isset($data['row'])&&$data['row']->status==1)?'checked':'' }} {{ isset($data['row'])?'':'checked' }} type="radio" class="ace">
                <span class="lbl"> Active</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="status" value="0" {{ (isset($data['row'])&&$data['row']->status==0)?'checked':'' }} type="radio" class="ace">
                <span class="lbl"> In-active</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>
@if(isset($data['row']))
    <script>

        $(document).ready(function () {
            $("#delete_banner_photo").click(function () {
                $.ajax({
                    type : 'POST',
                    url : '{{ url('banner/deleted') }}',
                    data : '{{ $data['row']->id }}',
                    data : "status="+status+"name="+name",
                    success : function (data) {
                        alert('success');
                    }
                });
            });
        });

    </script>
    <style>
        f{

        }
    </style>
    @endif