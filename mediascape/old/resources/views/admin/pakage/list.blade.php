@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - {{ trans('general.admin_panel') }} @endsection

@section('content')


    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            @include($view_path.'.partials.breadcrumb')

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $view_title }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'content.list.list') }}
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif

                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            sn
                                        </th>
                                        <th>Name</th>
                                        <th>status</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if ($data['rows']->count() > 0)
                                        <tr id="search_div">
                                            <td></td>
                                            <td><input type="text" id="search_name"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        @php $counter=0; @endphp
                                        @foreach($data['rows'] as $row)
                                            @php $counter++ @endphp

                                            <tr>
                                                <td class="center">
                                                    {{ $counter }}
                                                </td>

                                                <td>
                                                   {{ $row->title }}
                                                </td>

                                                <td>
                                                    @if ($row->status == 1)
                                                        Active
                                                    @else
                                                        InActive
                                                    @endif
                                                </td>



                                            @if(Auth::user()->type=="admin")
                                                <td>
                                                    <div class="btn-group">

                                                        <a class="btn btn-xs btn-info" href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                            <i class="icon-edit bigger-120"></i>
                                                        </a>



                                                    </div>

                                                    <div class="btn-group">

                                                        <a href="{{ route($base_route.'.delete', $row->id) }}"  class="btn btn-xs btn-danger bootbox-confirm">
                                                            <i class="icon-trash bigger-120"></i>
                                                        </a>



                                                    </div>


                                                </td>
                                                @endif
                                            </tr>

                                            @endforeach

                                        @else

                                        <tr>
                                            <td colspan="7">No data found.</td>
                                        </tr>

                                        @endif

                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-4">
                        {{ $data['rows']->links() }}
                            </div>

                            <!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

    @endsection

@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootbox.min.js') }}"></script>
    <script>
        $(".bootbox-confirm").on(ace.click_event, function() {
            console.log('sdfd');
            bootbox.confirm("Are you sure?", function(result) {
                if(result) {
                    //
                }
            });
        });
    </script>
    <script>

        $(document).ready(function () {
           $('#search_name').change(function () {

               var name = $('#search_name').val();

               var url = '{{ route('admin.banner.index') }}';

               $.ajax({
                   url: url,
                   method

               });

           });
        });

    </script>


@endsection