<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="title" id="title" value="{{ ViewHelper::getData('title', isset($data['santosh'])?$data['santosh']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

@if(isset($data['row']))

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="image"> Old Image </label>

        <lable class="col-sm-9">

            <img src="{{ asset('images/gallery/'.$data['row']['image']) }}" width="200px" for="image" alt="">
        </lable>
    </div>
    <div class="space-4"></div>

    <input type="hidden" value="{{ $data['row']['image'] }}" name="oldimg">
    @endif
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="image"> Image </label>

    <div class="col-sm-9">
        <input type="file" name="image" id="image" value="{{ ViewHelper::getData('image', isset($data['santosh'])?$data['santosh']:[]) }}" placeholder="Image" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status"> Status </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="status" value="1" checked type="radio" class="ace">
                <span class="lbl"> Active</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="status" value="0" type="radio" class="ace">
                <span class="lbl"> In-active</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>
