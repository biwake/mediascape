@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - {{ trans('general.admin_panel') }} @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('menu/general.menu.manager') }}</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ $view_title }} </a>
                </li>

                @if (isset($data))
                    <li class="active">{{ trans('admin/fmenu/list.top.edit') }}</li>
                @else
                    <li class="active">{{ trans('admin/fmenu/list.top.add') }}</li>
                @endif

            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $view_title }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/fmenu/list.top.list') }}
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif

                                {{--<table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            sn
                                        </th>
                                        <th>Name</th>
                                        <th>status</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if ($data['parent']->count() > 0)

                                        <tr id="search_div">
                                            <td></td>
                                            <td><input type="text" id="search_menu_name"></td>
                                            <td></td>

                                        </tr>
                                        @php $counter=0; @endphp
                                        @foreach($data['parent'] as $row)
                                            @php $counter++ @endphp

                                            <tr>

                                                <td class="center">
                                                    {{ $counter }}
                                                </td>

                                                <td>
                                                    {{ $row->title }}
                                                </td>

                                                <td>
                                                    @if ($row->status == 1)
                                                        Active
                                                    @else
                                                        InActive
                                                    @endif
                                                </td>



                                                @if(Auth::user()->type=="admin")
                                                    <td>
                                                        <div class="btn-group">

                                                            <a class="btn btn-xs btn-info" href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                                <i class="icon-edit bigger-120"></i>
                                                            </a>



                                                        </div>

                                                        <div class="btn-group">

                                                            <a href="{{ route($base_route.'.delete', $row->id) }}"  class="btn btn-xs btn-danger bootbox-confirm">
                                                                <i class="icon-trash bigger-120"></i>
                                                            </a>



                                                        </div>


                                                    </td>
                                                @endif
                                            </tr>

                                        @endforeach

                                    @else

                                        <tr>
                                            <td colspan="7">No data found.</td>
                                        </tr>

                                    @endif

                                    </tbody>
                                </table>
                                    <br>
                                    <br>
                                    <br>--}}
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            sn
                                        </th>
                                        <th>Name</th>
                                        <th>status</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if ($data['child']->count() > 0)
                                        @php $counter=0; @endphp
                                        @foreach($data['child'] as $row)
                                            @php $counter++ @endphp

                                            <tr>

                                                <td class="center">
                                                    {{ $counter }}
                                                </td>

                                                <td>
                                                    <a href="{{ url('admin/menu/'.$row->slug) }}">{{ $row->title }}</a>
                                                </td>

                                                <td>
                                                    @if ($row->status == 1)
                                                        Active
                                                    @else
                                                        InActive
                                                    @endif
                                                </td>



                                                @if(Auth::user()->type=="admin")
                                                    <td>
                                                        <div class="btn-group">

                                                            <a class="btn btn-xs btn-info" href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                                <i class="icon-edit bigger-120"></i>
                                                            </a>



                                                        </div>

                                                        <div class="btn-group">

                                                            <a href="{{ route($base_route.'.delete', $row->id) }}"  class="btn btn-xs btn-danger bootbox-confirm">
                                                                <i class="icon-trash bigger-120"></i>
                                                            </a>



                                                        </div>


                                                    </td>
                                                @endif
                                            </tr>

                                        @endforeach

                                    @else

                                        <tr>
                                            <td colspan="7">No data found.</td>
                                        </tr>

                                    @endif

                                    </tbody>
                                </table>
                                <div id="cart-item-wrapper"></div>
                            </div>
                            <div class="col-xs-4">
                            </div>

                            <!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection

@section('page_specific_scripts')

    <script>
        $('#search_menu_name').keyup(function () {

            var url = window.location;

            alert(url);

            // url = url + '?name=' + $('#search_menu_name').val();

//             location.href = url;

            {{--$.ajax({--}}
                {{--type : 'post',--}}
                {{--url : '{{ route('admin.menu.search') }}',--}}
                {{--data : {--}}
                    {{--_token : '{{ csrf_token() }}',--}}
                    {{--type : 'menu',--}}
                    {{--value : $('#search_menu_name').val()--}}
                {{--},--}}
                {{--success : function (response) {--}}
                    {{--var data = $.parseJSON(response);--}}
                    {{--if (data.error) {--}}
                        {{--alert('error');--}}
                    {{--} else {--}}
                        {{--$this.closest('td').find('input').val(new_qty);--}}

                        {{--$('#cart-item-wrapper').html(data.html);--}}

                        {{--alert('success');--}}
                    {{--}--}}

                {{--}--}}
            {{--});--}}
        });
    </script>

@endsection