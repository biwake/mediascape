<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="title" id="title" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

@if(!isset($data['row']))
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="slug"> Slug </label>

        <div class="col-sm-9">
            <input type="text" name="slug" id="slug" value="{{ ViewHelper::getData('slug', isset($data['row'])?$data['row']:[]) }}" placeholder="Slug" class="col-xs-10 col-sm-5">
        </div>
    </div>
    <div class="space-4"></div>
@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="link"> Link </label>

    <div class="col-sm-9">
        <input type="url" name="link" id="link" value="{{ ViewHelper::getData('link', isset($data['row'])?$data['row']:[]) }}" placeholder="Link" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="dropdown"> Dropdown </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="dropdown" value="1" id="dropdown_yes_js" {{ (isset($data['row'])&&$data['row']->parent_id==0)?'':'checked' }} {{ !isset($data['row'])?'checked':'' }} type="radio" class="ace">
                <span class="lbl"> Yes</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="dropdown" id="dropdown_no_js" {{ (isset($data['row'])&&$data['row']->parent_id==0)?'checked':'' }} value="0" type="radio" class="ace">
                <span class="lbl"> No</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>

@if(isset($data['menu']) && $data['menu']->count()>0)
<div class="form-group" id="parent_menu_js" style="display: {{ (isset($data['row'])&&$data['row']->parent_id==0)?'none':'' }};">
    <label class="col-sm-3 control-label no-padding-right" for="parent_id"> Parent Menu </label>

    <div class="col-sm-9">
        <select name="parent_id" id="parent_id" class="col-xs-10 col-sm-5">
            <option value="0">Null</option>
            @foreach($data['menu'] as $rew)
                <option {{ (isset($data['row']->parent)&&$data['row']->parent->id==$rew->id)?'selected':'' }} value="{{ $rew->id }}">{{ $rew->title }}</option>
                @endforeach
        </select>

    </div>
</div>
<div class="space-4"></div>
@endif

@if(isset($data['page']) && $data['page']->count()>0)
<div class="form-group" id="parent_menu_js">
    <label class="col-sm-3 control-label no-padding-right" for="page_id"> Page </label>

    <div class="col-sm-9">
        <select name="page_id" id="page_id" class="col-xs-10 col-sm-5">
            @foreach($data['page'] as $rew)
                <option {{ (isset($data['row']->page)&&$data['row']->page->id==$rew->id)?'selected':'' }} value="{{ $rew->id }}">{{ $rew->title }}</option>
                @endforeach
        </select>

    </div>
</div>
<div class="space-4"></div>
@endif

@if(isset($data['section']) && $data['section']->count()>0)
<div class="form-group" id="parent_menu_js">
    <label class="col-sm-3 control-label no-padding-right" for="section_id"> Section </label>

    <div class="col-sm-9">
        <select name="section_id" id="section_id" class="col-xs-10 col-sm-5">
            @foreach($data['section'] as $rew)
                <option {{ (isset($data['row'])&&$data['row']->section->id==$rew->id)?'selected':'' }} value="{{ $rew->id }}">{{ $rew->title }}</option>
                @endforeach
        </select>

    </div>
</div>
<div class="space-4"></div>
@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="target"> Target </label>

    <div class="col-sm-9">
        <select name="target" id="target" class="col-xs-10 col-sm-5">
            <option {{ (isset($data['row'])&&$data['row']->target==1)?'selected':'' }} value="1">Yes</option>
            <option {{ (isset($data['row'])&&$data['row']->target==0)?'selected':'' }} value="0">No</option>
        </select>

    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="order_by"> Order By </label>

    <div class="col-sm-9">
        <input type="number" name="order_by" id="order_by" value="{{ ViewHelper::getData('order_by', isset($data['row'])?$data['row']:[]) }}" placeholder="Order By" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status"> Status </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="status" value="1" {{ (isset($data['row'])&&$data['row']->status==1)?'checked':'' }} {{ isset($data['row'])?'':'checked' }} type="radio" class="ace">
                <span class="lbl"> Active</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="status" {{ (isset($data['row'])&&$data['row']->status==0)?'checked':'' }} value="0" type="radio" class="ace">
                <span class="lbl"> In-active</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>


<script>

    $(document).ready(function () {
        $('#title').keyup(function () {

            var title = $('#title').val();
            $('#slug').val(title.replace(/\s+/g, '-').toLowerCase());

        });

        $('#dropdown_yes_js').click(function () {
            $('#parent_menu_js').show(500);
        });
        $('#dropdown_no_js').click(function () {
            $('#parent_menu_js').hide(500);
        });


    });

</script>