<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="old-password"> Old Password </label>

    <div class="col-sm-9">
        <input type="password" name="old-password" id="old-password" value="{{ ViewHelper::getData('old-password', isset($data['row'])?$data['row']:[]) }}" placeholder="Old Password" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="new-password"> New Password </label>

    <div class="col-sm-9">
        <input type="password" name="new-password" id="new-password" value="{{ ViewHelper::getData('new-password', isset($data['row'])?$data['row']:[]) }}" placeholder="New Password" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="re-new-password"> Re New Password </label>

    <div class="col-sm-9">
        <input type="password" name="re-new-password" id="re-new-password" value="{{ ViewHelper::getData('re-new-password', isset($data['row'])?$data['row']:[]) }}" placeholder="Re New Password" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>
