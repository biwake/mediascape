<script src="{{ asset('assets/admin/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>
<script>
    $(document).ready(function () {

        $("#listing-form").validate({

            rules: {
                title: {
                    required: true,
                    minlength: 8
                },
                message: {
                    required: true,
                    max: 30
                }



            },
            messages: {
                title: {
                    required: "Please input a name.",
                    minlength: "Your name must be at least 8 characters long"
                },
                image: {
                    required: "Please input an image.",
                    minlength: "Your name must be at least 3 characters long",
                }
            }

        });

    });
</script>
