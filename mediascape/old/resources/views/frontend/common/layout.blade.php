<!DOCTYPE HTML>
<html>
<head>
    <title>{{ $profile->title }} | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Business_Blog Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    @include('frontend.common.cssscript')
    @yield('css')
</head>
<body>
<div class="spacil-wrapper" style="min-height: 700px;padding-bottom: 15px;">
    <div class="header col-md-12" style="margin-top: 2px;">
        @include('frontend.common.header')

        <!--head-bottom-->
        <div class="head-bottom">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            @include('frontend.common.menu')

        </div>
        <!--head-bottom-->
    </div>
    @yield('content')

    <div class="container">
        <div class="col-md-12" style="height: 40px;">
            <p style="text-align: center; margin-top: 10px">COPYRIGHT © {{ date("Y")}} <img src="{{ asset('frontend/images/logo.png') }}" height="30px" alt=""> & POWERED BY
                <a href="http://www.onlinemultimedia.com.np/" target="_blank">ONLINE MULTIMEDIA</a></p>
        </div>
    </div>
    <script>
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>
</div>
</body>
</html>