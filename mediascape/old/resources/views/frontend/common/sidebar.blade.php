<style>
    .dropdown_o:hover{
        color: #5F5E5C!important;
    }
</style>
<div class="container demo">

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        @php($counter = 0)
        @foreach($sidebar as $s)
            @php($counter++)

            @if($s->parent_id!=0)
                @continue
            @endif
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne" style="background-color: #5F5E5C;">
                    <h4 class="panel-title">
                        <a style="color: #ffffff;" id="accordion_btn{{ $counter }}" role="button" data-toggle="collapse" class="dropdown_a" data-parent="#accordion" href="#collapseOne{{ $counter }}" aria-expanded="true" aria-controls="collapseOne">
                            {{ $s->title }}
                            <i class="glyphicon glyphicon-plus" id="plus{{ $counter }}" class="plus_btn" style=""></i>
                            <i class="glyphicon glyphicon-minus" id="minus{{ $counter }}" class="minus_btn" style="float: right;display: none;"></i>
                        </a>
                    </h4>
                </div>
                <div id="collapseOne{{ $counter }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body" style="background-color: #C4262C;">
                        <ul style="padding-left: 10px;">
                            @foreach($s->children as $c)
                                <li style="list-style:decimal;color: #ffffff;" class="dropdown_o">
                                    <a style="color: #ffffff;" href="#" class="dropdown_o">
                                        {{ $c->title }}
                                    </a>
                                </li>
                                @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach

    </div>


</div>

<script>
    @php($counter = 0)
 @foreach($sidebar as $s)
     @php($counter++)

     @if($s->parent_id!=0)
         @continue
     @endif
$(document).ready(function () {
        $('#plus{{ $counter }}').click(function () {
            $('#plus{{ $counter }}').hide();
            $('#minus{{ $counter }}').show();
        });
        $('#minus{{ $counter }}').click(function () {
            $('#plus{{ $counter }}').show();
            $('#minus{{ $counter }}').hide();
        });
    });
    @endforeach
</script>