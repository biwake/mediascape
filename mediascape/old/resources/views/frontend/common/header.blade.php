<div class="header-top">
    <div class="logo">
        <a href="{{ route('frontend.home') }}">
            <img src="{{ asset('frontend/images/logo.png') }}" height="100px" alt="">
        </a>
    </div>
    <div class="social">
        <ul>
            @if(isset($profile->facebook))<li><a href="#" class="facebook"> </a></li>@endif
            @if(isset($profile->twitter))<li><a href="#" class="facebook twitter"> </a></li>@endif
            @if(isset($profile->youtube))<li><a href="#" class="facebook yout"> </a></li>@endif
        </ul>
    </div>
    <div class="clearfix"></div>
</div>