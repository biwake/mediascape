<style>
    .serve:hover{
        color: #C4262C!important;
    }
    .serve{
        color: #ffffff!important;
    }

    /* The container <div> - needed to position the dropdown content */
    .dropdown {
        position: relative;
        display: inline-block;
    }

    /* Dropdown Content (Hidden by Default) */
    .dropdown-content {
        display: none;
        position: absolute;
        z-index: 1;
    }

    /* Links inside the dropdown */
    .dropdown-content a {
        color: black;
        text-decoration: none;
        display: block;
    }

    /* Show the dropdown menu on hover */
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .activese a{
        color: #5F5E5C!important;
    }
</style>

<div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav col-md-11" style="margin-left: 10%;">
        {{--<li {!! Request::is('/')?'class="active"':''!!} ><a class="mainmenu" href="{{ route('frontend.home') }}">Home</a></li>
        <li {!! Request::is('about')?'class="active"':''!!} ><a class="mainmenu" href="{{ route('frontend.about') }}">About Us</a></li>
        @if(isset( $data['services']))
            <li {!! Request::is('action')?'class="active"':''!!} id="menu_li" class="dropdown">
                <a class="mainmenu" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                <ul class="dropdown-menu" id="menu_ul" style="background-color: #C4262C;">
                    @foreach($data['services'] as $service)
                        <li><a class="mainmenu" href="{{ url('service/'.$service->id) }}" style="font-size: 14px!important;" class="serve">{{ $service->title }}</a></li>
                        @endforeach

                </ul>
            </li>
        @endif--}}
        @foreach($menu as $m)
        <li class="{{ $m->children->count()>0?'dropdown mainmenu':'' }} {!! Request::is($m->page->slug.'*')?'activese':'' !!}">
            @if($m->parent_id!=0)
                @continue
                @endif
            <a href="{{ $m->children->count()>0?'#':(isset($m->page->slug)?route('frontend.'. $m->page->slug):'') }}" class="dropbtn mainmenu">{{ $m->title }}</a>
                @if($m->children->count()>0)
                <ul class="dropdown-content" style="background-color: #C4262C">
                    @foreach($m->children as $c)
                    <li style="list-style: none;"><a href="{{ route('frontend.ourteams', $c->page->slug) }}" class="mainmenu">{{ $c->title }}</a></li>
                        @endforeach
                </ul>
                    @endif
        </li>
        @endforeach
        {{--<li {!! Request::is('contacts')?'class="active"':''!!} ><a class="mainmenu" href="{{ route('frontend.contacts') }}">Contacts</a></li>
        <li {!! Request::is('portfolio')?'class="active"':''!!} ><a class="mainmenu" href="{{ route('frontend.portfolio') }}">Portfolio</a></li>--}}
    </ul>
</div><!--/.nav-collapse -->


<script>
    $(function(){
        $('#menu_li').hover(
            function() {
                $('#menu_ul').fadeIn();
            },
            function() {
                $('#menu_ul').fadeOut();
            });
    });

</script>