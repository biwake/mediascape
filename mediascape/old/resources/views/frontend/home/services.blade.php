@extends('frontend.common.layout')

@section('title') Services @endsection

@section('content')

    <div class="col-md-12" style="margin-top: 20px;margin-bottom: 40px;min-height: 500px;">
        {{--<div class="col-md-3">@include('frontend.common.sidebar')</div>--}}
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div style="color: #5F5E5C;">

                <h1 class="col-md-12" style="padding-bottom: 10px;color: #5F5E5C;text-align: center;">{{ $data['service']->title }}</h1>



                <div class="col-md-12">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <p style="text-align: justify!important;">
                            {!! $data['service']->description !!}
                        </p>
                    </div>
                    <div class="col-md-1"></div>
                </div>

                <div class="col-md-12">
                    @php($counter = 0)
                    @foreach($sidebar as $s)

                        @if($s->parent_id!=0)
                            @continue
                        @endif
                        @php($counter++)
                    <div class="col-md-4" style="margin-top: 30px;">
                        <div class="">
                            <div class="" role="tab" id="headingOne">
                                <h4 class="panel-title" style="background-color: #BA0F22;padding: 5px 15px;border-radius: 0 30xp 0 0">
                                    <a style="color: #ffffff;" id="accordion_btn{{ $counter }}" role="button" data-toggle="collapse" class="dropdown_a" data-parent="#accordion" href="#collapseOne{{ $counter }}" aria-expanded="true" aria-controls="collapseOne">
                                        {{ $s->title }}
                                    </a>
                                </h4>
                                <div class="" style="">
                                    <ul style="padding: 10px 0 0 10px;">
                                        @foreach($s->children as $c)
                                            <li style="list-style:none;color: #5F5E5C;" class="dropdown_o">
                                                <a style="color: #5F5E5C;" href="#" class="dropdown_o">
                                                    {{ $c->title }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="{{ ($counter%3==0)?'clearfix':'' }}"></div>
                    @endforeach
                </div>


            </div>
        </div>
        <div class="col-md-1"></div>
    </div>

    @endsection