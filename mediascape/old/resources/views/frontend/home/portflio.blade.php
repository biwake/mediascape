@extends('frontend.common.layout')
@section('title') Portflio @endsection
@section('content')
<div style="min-height: 500px;">
    <div class="container">
        <div class="col-md-12">

            <h3 style="text-align: center;padding: 30px 0 20px 0;font-size: 35px;color: #5f5e5c;">PORTFOLIO</h3>
            <div class="tcch col-md-12">
                <div class="tcch">

                    @php($counter = 0)
                    @foreach($data['gallery']  as $gallery)
                        @php($counter++)
                        <div class="col-md-6" style="margin-top: 50px;">
                            <div style="">
                                <a {{--href="#" --}}data-toggle="modal" data-target="#myModal">
                                    <img src="{{ asset('images/gallery/'.$gallery->image) }}" style="width: 90%;margin-left: 5%;height: 350px;border: 5px #D4D4D4 solid;" alt="">
                                </a>
                            </div>
                        </div>
{{--
                        <div class="modal fade" id="myModalhoasdf{{ $counter }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <img src="{{ asset('images/gallery/'.$gallery->image) }}" alt="">
                            </div>
                        </div>--}}
                    @endforeach


                    {{ $data['gallery']->links() }}
                </div>
                <!-- technology-top -->
            </div>
        </div>
    </div>
</div>
    @endsection