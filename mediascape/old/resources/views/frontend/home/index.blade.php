@extends('frontend.common.layout')
@section('title') Home @endsection
@section('content')
    <style>
        .media p{
            width: 80%;
            margin: 0 auto;
        }
    </style>
    @if (Request::session()->has('message'))
        <div class="container">
            <div class="col-md-12">
                <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="icon-remove"></i>
                    </button>
                    {!! Request::session()->get('message') !!}
                    <br>
                </div>
            </div>
        </div>
    @endif
    @if($data['banner'])
        <div class="container">
            <div class="col-md-12">

                <div class="tcch col-md-12">

                    <!-- technology-top -->


                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">

                            @php $counter = 0; @endphp
                            @foreach($data['banner'] as $banner)
                                @php $counter++; @endphp
                                <div class="item {{ $counter==1?'active':'' }}">
                                        <div class="tcch">

                                            <div class="tch-img col-md-5">
                                                <a href="singlepage.html"><img src="{{ asset('images/banner/'.$banner->image) }}" style="height: 400px;" class="img-responsive" alt=""/></a>
                                            </div>
                                            <div class="col-md-7">
                                                <p class="col-md-12 bannerp">We are <samp>BRAND</samp> architects..........</p>
                                                <p class="col-md-12 div">We care.... We share.... every bit of knowledge through our <samp>service</samp></p>
                                            </div>
                                        </div>
                                </div>
                                @endforeach

                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>




                </div>
            </div>
        </div>
        @endif
    <div class="container">
        <div class="head-bottom-black">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                @if(isset($data['second_menu']) && count($data['second_menu'])>0)
                    <ul class="nav navbar-nav col-md-11" style="margin-left: 30%">
                        {{--<li class="active"><a href="index.html">Home</a></li>
                        <li class="below-navbar"><a class="secondmenu" href="index.html" class="a-below-navbar">Design</a></li>
                        <li class="below-navbar"><a class="secondmenu" href="videos.html" class="a-below-navbar">advertising</a></li>
                        <li class="below-navbar"><a class="secondmenu" href="reviews.html" class="a-below-navbar">events</a></li>--}}
                        @foreach($data['second_menu'] as $sm)
                            <li class="below-navbar"><a class="secondmenu" href="design.html" class="a-below-navbar">{{ $sm->title }}</a></li>
                            @endforeach
                    </ul>
                    @endif
                <div class="col-md-1"></div>
            </div><!--/.nav-collapse -->
        </div>
        <div class="col-md-12">
            @if(isset($data['about']))
                <div class="col-md-12 media">
                    <h3>Welcome To Media Scape</h3>
                    <p style="">{!! str_limit($data['about']->description,500) !!}</p>
                    <a href="{{ route('frontend.about-us') }}" class="readmore">Read More</a>

                </div>
            @endif
            <div class="col-md-8 bannersecond">
                <img src="{{ asset('frontend/images/banner-1.jpg') }}" class="col-md-12" alt="">
                <div>
                    <img src="{{ asset('frontend/images/logo.png') }}" style="float: left;height: 50px; " >
                    <p style="padding-top: 20px;">more ideas than other</p>
                </div>
            </div>
            <div class="col-md-4">
                <form action="{{ route('frontend.onlineorder') }}" method="post" class="onlineorder">
                    {{ csrf_field() }}
                    <table class="onlineorderform" style="margin-top: 70px;">
                        <tr>
                            <td colspan="2"><h2 class="form_h" style="background: #a4a4a4;
    width: 54%;
    padding: 5px 0 5px 43px;
    color: white;
    position: absolute;
    left: 5px;
    font-size:25px;
    top: 40px;">Online Order</h2></td>
                        </tr>
                        <tr>
                            <td style="padding-top: 10px;"><label style="color: white;">Name : </label></td>
                            <td style="padding-top: 10px;"><input  type="text" class="inputfell" name="username"></td>
                        </tr>
                        <tr>
                            <td style="padding-top: 10px;"><label style="color: white;">Email : </label></td>
                            <td style="padding-top: 10px;"><input class="inputfell" type="email" name="email"></td>
                        </tr>
                        <tr>
                            <td style="padding-top: 10px;"><label style="color: white;">Package : </label></td>
                            <td style="padding-top: 10px;">
                                <select name="pakege" class="inputfell">
                                    @if(isset($data['pakage']))
                                        @foreach($data['pakage'] as $pakage)
                                            <option value="{{ $pakage->title }}">{{ $pakage->title }}</option>
                                            @endforeach
                                        @else
                                        <option>Design Pakege</option>
                                    @endif
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 10px;"><label style="color: white;">Message : </label></td>
                            <td style="padding-top: 10px;"><textarea name="message" class="inputfell"></textarea></td>
                        </tr>
                        <th>
                        <td colspan="2"><input class="inputfell" type="submit" style="background: #F6E607; float: right; color: #85B88C; padding: 3px 15px; border: none;"></td>
                        </th>
                    </table>

                </form>
            </div>
        </div>
    </div>
    @if(isset($data['gallery']))
        <div class="container" style="margin-bottom: 10px;">
            <div class="head-bottom">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <p class="portfolio">PORTFOLIO</p>
                </div><!--/.nav-collapse -->
            </div>
            <div class="col-md-12">

                <div class="tcch col-md-12">
                    <div class="tcch">
                        @php($counter = 0)
                        @foreach($data['gallery']  as $gallery)
                            <div class="col-md-6" style="margin-top: 50px;">
                                <div style="">
                                    <a {{--href="#" --}}data-toggle="modal" data-target="#myModal">
                                        <img src="{{ asset('images/gallery/'.$gallery->image) }}" style="width: 90%;margin-left: 5%;height: 350px;border: 5px #D4D4D4 solid;" alt="">
                                    </a>
                                </div>
                            </div>


                            {{--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                        </div>
                                        <div class="modal-body">
                                            ...
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </div>--}}
                            @endforeach
                    </div>
                    <!-- technology-top -->
                </div>
                <p class="col-md-12" style="text-align: center;padding-top: 10px;">
                    <a href="{{ route('frontend.portfolio') }}" class="readmore">See More</a>
                </p>
            </div>
        </div>
    @endif


    @endsection