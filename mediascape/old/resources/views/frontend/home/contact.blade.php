@extends('frontend.common.layout')
@section('title') Contact @endsection
@section('content')
    <style>
        .san{
            background-color: #C4262C;
            padding: 5px 15px;
            border:none;
            border-radius: 2px;
            color: #ffffff;
        }
        .san:hover{
            background-color: #5F5E5C;
        }
    </style>

    <div style="min-height: 500px; padding-top: 150px;">
        <h2 style="text-align: center; color: #5f5f5c">GET IN TOUCH</h2>
        <div class="col-md-12" style="margin-bottom: 10px;">
            <div class="col-md-1"></div>
                <div class="col-md-3" style="margin-top: 10px;">
                    <form action="">
                        <ul>
                            <li style="list-style: none;margin-right: 10px;margin-bottom: 10px;">
                                 <h1 style="color: #5f5f5c">{{ isset($profile->title)?$profile->title:'' }}</h1>
                            </li>
                            <li style="list-style: none;margin-right: 10px;margin-bottom: 10px;">
                                <i class="glyphicon glyphicon-map-marker" style="color: #5f5f5c"></i>  {{ isset($profile->address)?$profile->address:'' }}
                            </li>
                            <li style="list-style: none;margin-right: 10px;margin-bottom: 10px;">
                                <i class="glyphicon glyphicon-phone-alt" style="color: #5f5f5c"></i> {{ isset($profile->phone)?$profile->phone:'' }}
                            </li>
                            <li style="list-style: none;margin-right: 10px;margin-bottom: 10px;">
                                <i class="glyphicon glyphicon-earphone" style="color: #5f5f5c"></i> {{ isset($profile->mobile)?$profile->mobile:'' }}
                            </li>
                            <li style="list-style: none;margin-right: 10px;margin-bottom: 10px;">
                                <i class="glyphicon glyphicon-envelope" style="color: #5f5f5c"></i> {{ isset($profile->email)?$profile->email:'' }}
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="col-md-4">
                    <form action="">
                        <table>
                            <tr style="height: 70px;">
                                <td>
                                    <lable for="name">Name :</lable>
                                </td>
                                <td>
                                    <input id="name" class="form-control" type="text" name="name" style="width: 300px;">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <lable for="email">Email :</lable>
                                </td>
                                <td>
                                    <input id="email" class="form-control" type="email" name="email">
                                </td>
                            </tr>
                            <tr style="height: 155px;">
                                <td>
                                    <lable for="message">Message :</lable>
                                </td>
                                <td>
                                    <textarea id="message" class="form-control" rows="5" name="message"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <input class="san" type="submit" name="send_btn" value="Send">
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="col-md-3" style="margin-top: 15px;">
                    <div class="fb-page" data-href="https://www.facebook.com/mediascapepl/" data-small-header="false"
                         data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/mediascapepl/" class="fb-xfbml-parse-ignore"><a
                                    href="https://www.facebook.com/mediascapepl/">Mediascape Pvt. Ltd.</a></blockquote>
                    </div>
                </div>
            <div class="col-md-1"></div>

        <div class="col-md-12" style="margin-top: 22px;">
            <div id="g-map">
                <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key= AIzaSyBD7PzmUXIRWBx29SsfouCR25MZ8lHJFJs '></script>
                <div style='overflow:hidden;/*height:390px;width:620px;*/'>
                    <div id='gmap_canvas' style='height:400px;width: 100%;'></div>
                    <style type="text/css">#gmap_canvas img {
                            max-width: none !important;
                            background: none !important
                        }</style>
                </div>
                <script type='text/javascript'>
                    function init_map() {
                        var myOptions = {
                            zoom: 13,
                            center: new google.maps.LatLng(27.690197, 85.318374),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
                        marker = new google.maps.Marker({
                            map: map,
                            position: new google.maps.LatLng(27.690197, 85.318374)
                        });

                        infowindow = new google.maps.InfoWindow({content: 'Mediascape Pvt. Ltd.'});
                        google.maps.event.addListener(marker, 'click', function () {
                            infowindow.open(map, marker);
                        });
                        infowindow.open(map, marker);
                    }
                    google.maps.event.addDomListener(window, 'load', init_map);
                </script>
            </div>
        </div>

    </div>

    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=907481089393484";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>



@endsection