<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('title',100)->unique();
            $table->string('slug',100)->unique();
            $table->string('link')->nullable();
            $table->integer('parent_id')->default(0);
            $table->integer('page_id');
            $table->integer('section_id');
            $table->boolean('target')->default(0);
            $table->boolean('status')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu');
    }
}
