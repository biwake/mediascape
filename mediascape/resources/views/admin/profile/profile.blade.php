@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - {{ trans('general.admin_panel') }} @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route($base_route) }}">{{ $view_title }} </a>
                </li>

                @if (isset($data))
                    <li class="active">{{ trans($trans_path.'content.common.edit') }} </li>
                @else
                    <li class="active">{{ trans($trans_path.'content.common.add') }} </li>
                @endif

            </ul><!-- .breadcrumb -->

        </div>
        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $view_title }}<small>
                        <i class="icon-double-angle-right"></i>
                        Edit</small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route($base_route.'.updatePassword',Auth::user()->id) }}" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <h1>Change Password</h1>

                        @include($view_path.'partials.form1')
                        @if(Session::get('message'))
                            <p class="alert alert-success">{{ Session::get('message') }}</p>
                        @endif
                        @if(Session::get('error-message'))
                            <p class="alert alert-danger">{{ Session::get('error-message') }}</p>
                        @endif
                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="icon-ok bigger-110"></i>
                                    {{ trans('general.button.update') }}
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="icon-undo bigger-110"></i>
                                    {{ trans('general.button.reset') }}
                                </button>
                            </div>
                        </div>

                        <div class="hr hr-24"></div>



                    </form>


                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

    @endsection
@section('page_specific_scripts')

    @include($view_path.'.partials.add.jquery-validation-scripts')

@endsection