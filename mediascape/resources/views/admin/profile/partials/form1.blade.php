<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="old_password"></label>

    <div class="col-sm-9">
        @if(Session::get('success-message'))
            <p style="color: darkcyan;">{{ Session::get('success-message') }}</p>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="old_password"> Old Password </label>

    <div class="col-sm-9">
        <input type="password" name="old_password" id="old_password" value="{{ ViewHelper::getData('old_password', isset($data['row'])?$data['row']:[]) }}" placeholder="Old Password" class="col-xs-10 col-sm-5">
        @if(Session::get('old-error-message'))
            <p style="color: darkred;">{{ Session::get('old-error-message') }}</p>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="new_password"> New Password </label>

    <div class="col-sm-9">
        <input type="password" name="new_password" id="new_password" value="{{ ViewHelper::getData('new_password', isset($data['row'])?$data['row']:[]) }}" placeholder=" New Password " class="col-xs-10 col-sm-5">
        @if(Session::get('new-error-message'))
            <p style="color: darkred;">{{ Session::get('new-error-message') }}</p>
        @endif
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="re_new_password"> Re-New Password </label>

    <div class="col-sm-9">
        <input type="password" name="re_new_password" id="re_new_password" value="{{ ViewHelper::getData('re_new_password', isset($data['row'])?$data['row']:[]) }}" placeholder=" Re-New Password" class="col-xs-10 col-sm-5">
        @if(Session::get('new-error-message'))
            <p style="color: darkred;">{{ Session::get('new-error-message') }}</p>
        @endif
    </div>
</div>
<div class="space-4"></div>