<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="title" id="title" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>


@if(isset($data['row']))

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="image"> Old Logo </label>

        <lable class="col-sm-9">

            <img src="{{ asset('images/profile/'.$data['row']->image) }}" width="200px" for="image" alt="">
        </lable>
    </div>
    <div class="space-4"></div>

    <input type="hidden" value="{{ $data['row']['image'] }}" name="oldimg">
@endif


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="image"> Logo </label>

    <div class="col-sm-9">
        <input type="file" name="image" id="image" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="address"> Address </label>

    <div class="col-sm-9">
        <input type="text" name="address" id="address" value="{{ ViewHelper::getData('address', isset($data['row'])?$data['row']:[]) }}" placeholder="Address" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="phone"> Phone </label>

    <div class="col-sm-9">
        <input type="text" name="phone" id="phone" value="{{ ViewHelper::getData('phone', isset($data['row'])?$data['row']:[]) }}" placeholder="Phone" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="mobile"> Mobile </label>

    <div class="col-sm-9">

        <input type="text" name="mobile" id="mobile" value="{{ ViewHelper::getData('mobile', isset($data['row'])?$data['row']:[]) }}" placeholder="Mobile" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="email"> Email </label>

    <div class="col-sm-9">
        <input type="Email" name="email" id="email" value="{{ ViewHelper::getData('email', isset($data['row'])?$data['row']:[]) }}" placeholder="Email" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="facebook"> Facebook </label>

    <div class="col-sm-9">

        <input type="text" name="facebook" id="facebook" value="{{ ViewHelper::getData('facebook', isset($data['row'])?$data['row']:[]) }}" placeholder="Facebook" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="twitter"> Twitter </label>

    <div class="col-sm-9">

        <input type="text" name="twitter" id="twitter" value="{{ ViewHelper::getData('twitter', isset($data['row'])?$data['row']:[]) }}" placeholder="Twitter" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="youtube"> Youtube </label>

    <div class="col-sm-9">

        <input type="text" name="youtube" id="youtube" value="{{ ViewHelper::getData('youtube', isset($data['row'])?$data['row']:[]) }}" placeholder="Youtube" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>
