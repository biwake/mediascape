<div class="navbar navbar-default" style="background-color: #fff!important;border-bottom: 1px #3d3d3d solid;" id="navbar">
    <script type="text/javascript">
        try{ace.settings.check('navbar' , 'fixed')}catch(e){}
    </script>

    <div class="navbar-container" id="navbar-container">
        <div class="navbar-header pull-left">
            <a href="#" class="navbar-brand">
                <small style="color:#333;">
                    {{--<img src="{{ asset('images/profile/'.$profile->image) }}" height="25px" alt="">--}}
                    Media <span style="color: #BA0F22;">Scape</span>
                </small>
            </a><!-- /.brand -->
        </div><!-- /.navbar-header -->

        <div class="navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">

                <li class="green">
                    <a target="_blank" href="{{ route('index') }}">
                        Visit Website
                    </a>
                </li>

                <li class="red">
                    <a href="{{ route('admin.profile') }}">
                        <i class="icon-user"></i>
                    </a>
                </li>

                <li class="blue">
                    <a href="{{ route('admin.profile.setting') }}">
                        <i class="icon-cog"></i>
                    </a>
                </li>

                <li class="light-blue">
                    <a href="{{ route('logout') }}">
                        <i class="icon-signout"></i>
                    </a>
                </li>
            </ul><!-- /.ace-nav -->
        </div><!-- /.navbar-header -->
    </div><!-- /.container -->
</div>