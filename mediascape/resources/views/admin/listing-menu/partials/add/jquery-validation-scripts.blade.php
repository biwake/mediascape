<script src="{{ asset('assets/admin/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>
<script>
    $(document).ready(function () {

        $("#listing-form").validate({

            rules: {
                title: {
                    required: true
                }



            },
            messages: {
                title: {
                    required: "Please input a name.",
                    minlength: "Your name must be at least 8 characters long"
                },
                image: {
                    required: "Please input an image.",
                    type: "You cha only upload image file type",
                }
            }

        });

    });
</script>
