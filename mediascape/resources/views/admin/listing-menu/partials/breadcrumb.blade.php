<ul class="breadcrumb">
    <li>
        <i class="icon-home home-icon"></i>
        <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
    </li>

    <li>
        <a href="{{ route($base_route.'.index',$data['slug']) }}">{{ $view_title }} </a>
    </li>

    @if (isset($data))
        <li class="active">Edit </li>
    @else
        <li class="active">Add </li>
    @endif

</ul><!-- .breadcrumb -->