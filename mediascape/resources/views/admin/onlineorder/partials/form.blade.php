<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="title" id="title" value="{{ ViewHelper::getData('title', isset($data['santosh'])?$data['santosh']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="message"> Message </label>

    <div class="col-sm-9">
        <textarea name="message" id="message" placeholder="Message" class="col-xs-10 col-sm-5">{{ ViewHelper::getData('short_description', isset($data['santosh'])?$data['santosh']:[]) }}</textarea>
        <script>
            CKEDITOR.replace( 'description', {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
            });
        </script>
    </div>
</div>
<div class="space-4"></div>
