@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - {{ trans('general.admin_panel') }} @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="#">{{ $view_title }} </a>
                </li>

                @if (isset($data))
                    <li class="active">List</li>
                @else
                    <li class="active">Add</li>
                @endif

            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">

                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $view_title }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/fmenu/list.top.list') }}
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif

                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            sn
                                        </th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Pakage</th>
                                        <th>Message</th>
                                        <th>status</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if ($data['rows']->count() > 0)

                                        <tr id="search_div">
                                            <td></td>
                                            <td><input type="text" id="search_name"></td>

                                        </tr>
                                        @php $counter=0; @endphp
                                        @foreach($data['rows'] as $row)
                                            @php $counter++ @endphp

                                            <tr>

                                                <td class="center">
                                                    {{ $counter }}
                                                </td>

                                                <td>
                                                    {{ $row->username }}
                                                </td>

                                                <td>
                                                    {{ $row->email }}
                                                </td>

                                                <td>
                                                    {{ $row->pakage_id }}
                                                </td>

                                                <td>
                                                    {{ $row->message }}
                                                </td>

                                                <td>
                                                    @if ($row->status == 1)
                                                        <a href="{{ route($base_route.'.update', $row->id) }}" class="btn btn-success">Active</a>
                                                    @else
                                                        <a href="{{ route($base_route.'.update', $row->id) }}" class="btn btn-info">InActive</a>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ route($base_route.'.reply', $row->id) }}" class="btn btn-info"><i class="icon-reply"></i></a>
                                                    <a href="{{ route($base_route.'.delete', $row->id) }}" class="btn btn-danger"><i class="icon-remove"></i></a>
                                                </td>

                                            </tr>

                                        @endforeach

                                    @else

                                        <tr>
                                            <td colspan="7">No data found.</td>
                                        </tr>

                                    @endif

                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-4">
                                {{ $data['rows']->links() }}
                            </div>

                            <!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection

@section('page_specific_scripts')

@endsection