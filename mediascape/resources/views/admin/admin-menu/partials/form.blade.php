<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9" class="col-xs-10 col-sm-5">
        <input type="text" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" name="title" id="title">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Status </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="status" value="1" {{ (isset($data['row'])&&$data['row']->status==1)?'checked':'' }} {{ isset($data['row'])?'':'checked' }} type="radio" class="ace">
                <span class="lbl"> Active</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="status" value="0" {{ (isset($data['row'])&&$data['row']->status==0)?'checked':'' }} type="radio" class="ace">
                <span class="lbl"> In-active</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>
@if(isset($data['row']))
    <script>

        /*$(document).ready(function () {
            $("#delete_banner_photo").click(function () {
                $.ajax({
                    type : 'POST',
                    url : '{{ url('banner/deleted') }}',
                    data : '{{ $data['row']->id }}',
                    data : "status="+status+"name="+name",
                    success : function (data) {
                        alert('success');
                    }
                });
            });
        });*/

    </script>
    @endif
<script type="text/javascript">
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#upload_image').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function removeImageView() {
        var $image = $('#upload_image');
        $image.removeAttr('src').replaceWith($image.clone());
    }

    $("#image").change(function(){
        var filesize = this.files[0].size;
        var filesizeinMb = Math.round(filesize/1024);
        if (filesizeinMb > 2046){
            alert('Please Upload image file less than 2 MB')
            removeImageView();
        }
        else {
            readURL(this);
        }

    });

</script>
<script>
    $('#reset').click(function () {
        removeImageView();
    });
</script>