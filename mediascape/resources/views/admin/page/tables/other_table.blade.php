<table id="sample-table-1" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th class="center">
            sn
        </th>
        <th>Type of page</th>
        {{--<th>Demo</th>--}}
        <th>List</th>
        <th></th>
    </tr>
    </thead>

    <tbody>
    <tr>
        <td class="center">
            1
        </td>
        <td>Simple page</td>
        {{--<td>Demo</td>--}}
        <td><a href="{{ route('admin.page.index',['page_type'=>'simple_page']) }}" class="btn btn-xs btn-info">List of Pages</a></td>
        <td><a class="btn btn-xs btn-success" href="{{ route('admin.page.add',['page_type'=>'simple_page']) }}">Create</a></td>
    </tr>
    <tr>
        <td class="center">
            2
        </td>
        <td>Image Page</td>
        {{--<td>Demo</td>--}}
        <td><a href="{{ route('admin.page.index',['page_type'=>'image_page']) }}" class="btn btn-xs btn-info">List of Pages</a></td>
        <td><a class="btn btn-xs btn-success" href="{{ route('admin.page.add',['page_type'=>'image_page']) }}">Create</a></td>
    </tr>
    <tr>
        <td class="center">
            3
        </td>
        <td>Simple listing page</td>
        {{--<td>Demo</td>--}}
        <td><a href="{{ route('admin.page.index',['page_type'=>'simple_listing_page']) }}" class="btn btn-xs btn-info">List of Pages</a></td>
        <td><a class="btn btn-xs btn-success" href="{{ route('admin.page.add',['page_type'=>'simple_listing_page']) }}">Create</a></td>
    </tr>
    <tr>
        <td class="center">
            4
        </td>
        <td>Image listing Page</td>
        {{--<td>Demo</td>--}}
        <td><a href="{{ route('admin.page.index',['page_type'=>'image_listing_page']) }}" class="btn btn-xs btn-info">List of Pages</a></td>
        <td><a class="btn btn-xs btn-success" href="{{ route('admin.page.add',['page_type'=>'image_listing_page']) }}">Create</a></td>
    </tr>
    </tbody>
</table>