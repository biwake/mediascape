<table id="sample-table-1" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th class="center">
            sn
        </th>
        <th>Name</th>
        <th>status</th>
        <th></th>
    </tr>
    </thead>

    <tbody>
    @if ($data['rows']->count() > 0)
        @php $counter=0; @endphp
        @foreach($data['rows'] as $row)
            @php $counter++ @endphp

            <tr>

                <td class="center">
                    {{ $counter }}
                </td>

                <td>
                    {{ $row->title }}
                </td>

                <td>
                    @if ($row->status == 1)
                        <a class="btn btn-xs btn-success" href="{{ route('admin.page.status.update',$row->id) }}">Active</a>
                    @else
                        <a class="btn btn-xs btn-danger" href="{{ route('admin.page.status.update',$row->id) }}">InActive</a>
                    @endif
                </td>



                @if(Auth::user()->type=="admin")
                    <td>
                        <div class="btn-group">

                            <a class="btn btn-xs btn-info" href="{{ route($base_route.'.edit', ['id' => $row->id,'page_type' => $page_type]) }}">
                                <i class="icon-edit bigger-120"></i>
                            </a>



                        </div>

                        <div class="btn-group">

                            <a href="{{ route($base_route.'.delete',['id' => $row->id,'page_type' => $page_type]) }}"  class="btn btn-xs btn-danger bootbox-confirm">
                                <i class="icon-trash bigger-120"></i>
                            </a>



                        </div>

                        <div class="btn-group">

                            <a href="{{ route('admin.listing-menu.add',['val' => $row->slug]) }}"  class="btn btn-xs btn-success bootbox-confirm">
                                <i class="icon-plus bigger-120"></i>
                            </a>

                        </div>


                    </td>
                @endif
            </tr>

        @endforeach

    @else

        <tr>
            <td colspan="7">No data found.</td>
        </tr>

    @endif

    </tbody>
</table>