@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - {{ trans('general.admin_panel') }} @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.demo') }}"> {{ ucwords(str_replace('_',' ',$page_type)) }} </a>
                </li>
                    <li class="active">List</li>

            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                {{--<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>--}}
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ ucwords(str_replace('_',' ',$page_type)) }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/fmenu/list.top.list') }}
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif

                                    <td><a href="{{ route('admin.page.demo') }}" class="btn btn-sm btn-info">Back</a></td>
                                    <td><a class="btn btn-sm btn-success" href="{{ route('admin.page.add',['page_type'=>$page_type]) }}">Create a new page</a></td>

                                    @include($view_path.'tables.'.$page_type.'_form')
                            </div>
                            <div class="col-xs-4">

                            </div>

                            <!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif

                                    <td><p class="btn btn-sm btn-danger">Other Layout's Page</p></td>

                                    @include($view_path.'tables.'.'other_table')
                            </div>
                            <div class="col-xs-4">

                            </div>

                            <!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection

@section('page_specific_scripts')

    <script>
        $.ajax({
            type : 'post',
            url : ''
        });
    </script>

@endsection