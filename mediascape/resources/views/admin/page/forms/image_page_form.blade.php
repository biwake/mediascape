<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9" class="col-xs-10 col-sm-5">
        <input type="text" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" name="title" id="title">
    </div>
</div>
<div class="space-4"></div>

@if(isset($data['row']))

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="image"> Old Image </label>

        <lable class="col-sm-9">

            <img src="{{ asset('images/page/'.$data['row']['image']) }}" width="200px" for="image" alt="">
            <input type="button" id="delete_banner_photo">
        </lable>
    </div>
    <div class="space-4"></div>

    <input type="hidden" value="{{ $data['row']['image'] }}" name="oldimg">
@endif

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="image"> Image </label>

    <div class="col-sm-9">
        <input type="file" name="image" id="image" value="{{ ViewHelper::getData('image', isset($data['row'])?$data['row']:[]) }}" placeholder="Image" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="image"></label>

    <div class="col-sm-9">
        <img src="" id="upload_image" style="width: 100px" alt="">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="description"> description </label>

    <div class="col-sm-9">
        <textarea name="description" id="description" placeholder="description" class="col-xs-10 col-sm-5">{{ ViewHelper::getData('description', isset($data['row'])?$data['row']:[]) }}</textarea>
        <script>
            CKEDITOR.replace( 'description', {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
            });
        </script>
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Status </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="status" value="1" {{ (isset($data['row'])&&$data['row']->status==1)?'checked':'' }} {{ isset($data['row'])?'':'checked' }} type="radio" class="ace">
                <span class="lbl"> Active</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="status" value="0" {{ (isset($data['row'])&&$data['row']->status==0)?'checked':'' }} type="radio" class="ace">
                <span class="lbl"> In-active</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>
@if(isset($data['row']))
    <script>

        /*$(document).ready(function () {
         $("#delete_banner_photo").click(function () {
         $.ajax({
         type : 'POST',
         url : '{{ url('banner/deleted') }}',
         data : '{{ $data['row']->id }}',
         data : "status="+status+"name="+name",
         success : function (data) {
         alert('success');
         }
         });
         });
         });*/

    </script>
@endif
<script type="text/javascript">
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#upload_image').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function removeImageView() {
        var $image = $('#upload_image');
        $image.removeAttr('src').replaceWith($image.clone());
    }

    $("#image").change(function(){
        var filesize = this.files[0].size;
        var filesizeinMb = Math.round(filesize/1024);
        if (filesizeinMb > 2046){
            alert('Please Upload image file less than 2 MB')
            removeImageView();
        }
        else {
            readURL(this);
        }

    });

</script>
<script>
    $('#reset').click(function () {
        removeImageView();
    });
</script>