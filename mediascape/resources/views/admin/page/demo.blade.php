@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - {{ trans('general.admin_panel') }} @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.demo') }}">{{ $view_title }} </a>
                </li>

                    <li class="active">Demo</li>

            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">

                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $view_title }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        Demo
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif

                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            sn
                                        </th>
                                        <th>Type of page</th>
                                        {{--<th>Demo</th>--}}
                                        <th>List</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td class="center">
                                            1
                                        </td>
                                        <td>Simple page</td>
                                        {{--<td>Demo</td>--}}
                                        <td><a href="{{ route('admin.page.index',['page_type'=>'simple_page']) }}" class="btn btn-xs btn-info">List of Pages</a></td>
                                        <td><a class="btn btn-xs btn-success" href="{{ route('admin.page.add',['page_type'=>'simple_page']) }}">Create</a></td>
                                    </tr>
                                    <tr>
                                        <td class="center">
                                            2
                                        </td>
                                        <td>Image Page</td>
                                        {{--<td>Demo</td>--}}
                                        <td><a href="{{ route('admin.page.index',['page_type'=>'image_page']) }}" class="btn btn-xs btn-info">List of Pages</a></td>
                                        <td><a class="btn btn-xs btn-success" href="{{ route('admin.page.add',['page_type'=>'image_page']) }}">Create</a></td>
                                    </tr>
                                    <tr>
                                        <td class="center">
                                            3
                                        </td>
                                        <td>Simple listing page</td>
                                        {{--<td>Demo</td>--}}
                                        <td><a href="{{ route('admin.page.index',['page_type'=>'simple_listing_page']) }}" class="btn btn-xs btn-info">List of Pages</a></td>
                                        <td><a class="btn btn-xs btn-success" href="{{ route('admin.page.add',['page_type'=>'simple_listing_page']) }}">Create</a></td>
                                    </tr>
                                    <tr>
                                        <td class="center">
                                            4
                                        </td>
                                        <td>Image listing Page</td>
                                        {{--<td>Demo</td>--}}
                                        <td><a href="{{ route('admin.page.index',['page_type'=>'image_listing_page']) }}" class="btn btn-xs btn-info">List of Pages</a></td>
                                        <td><a class="btn btn-xs btn-success" href="{{ route('admin.page.add',['page_type'=>'image_listing_page']) }}">Create</a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-4">

                            </div>

                            <!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection

@section('page_specific_scripts')

    <script>
        $.ajax({
            type : 'post',
            url : ''
        });
    </script>

@endsection