<ul class="breadcrumb">
    <li>
        <i class="icon-home home-icon"></i>
        <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
    </li>

    <li>
        <a href="{{ route($base_route.'.index') }}">{{ $view_title }} </a>
    </li>

        <li class="active">List </li>

</ul><!-- .breadcrumb -->