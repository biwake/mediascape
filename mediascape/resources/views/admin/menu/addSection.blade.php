@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - {{ trans('general.admin_panel') }} @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ $view_title }} </a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ $title }} </a>
                </li>

                <li class="active">Set Section </li>

            </ul><!-- .breadcrumb -->

        </div>

        <div class="page-content">
            <div class="page-header">

                <a href="javascript:history.back()" class="btn btn-grey">
                    <i class="ace-icon fa fa-arrow-left"></i>
                    Go Back
                </a>

            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    {{--@if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif--}}

                    @if (Request::session()->has('info'))
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="icon-remove"></i>
                            </button>
                            {!! Request::session()->get('info') !!}
                            <br>
                        </div>
                    @endif

                    <form id="listing-form" class="form-horizontal" role="form" method="POST" action="{{ route($base_route.'.store.section',$id) }}" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="section"> Session </label>

                            <div class="col-sm-9">
                                @if(isset($sections) && count($sections))
                                    @php($counter = 0)
                                    @foreach($sections as $s)
                                        @php($counter++)

                                        <div class="radio">
                                            <label>
                                                <input name="section{{ $counter }}" value="{{ $s->id }}" {{ in_array($s->id,$menu_sections)?'checked':'' }} type="checkbox" class="ace">
                                                <span class="lbl"> {{ $s->title }}</span>
                                            </label>
                                        </div>

                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="space-4"></div>

                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button id="form-submit-btn" class="btn btn-info" type="submit">
                                    <i class="icon-ok bigger-110"></i>
                                    {{ trans('general.button.submit') }}
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="icon-undo bigger-110"></i>
                                    {{ trans('general.button.reset') }}
                                </button>
                            </div>
                        </div>

                        <div class="hr hr-24"></div>



                    </form>


                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection

@section('page_specific_scripts')

    @include($view_path.'.partials.add.jquery-validation-scripts')

@endsection