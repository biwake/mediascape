@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - {{ trans('general.admin_panel') }} @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ $view_title }} </a>
                </li>

                @if (isset($data))
                    <li class="active">Edit </li>
                @else
                    <li class="active">Add </li>
                @endif

            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">

                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $view_title }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/fmenu/list.top.list') }}
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif

                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            sn
                                        </th>
                                        <th>Name</th>
                                        <th>Page Link</th>
                                        <th>Section Manage</th>
                                        <th>status</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if ($data['menu']->count() > 0)
                                        @php $counter=0; @endphp
                                        @foreach($data['menu'] as $row)
                                            @php $counter++ @endphp

                                            <tr>

                                                <td class="center">
                                                    {{ $counter }}
                                                </td>

                                                <td>
                                                    {{ $row->title }}
                                                    <div style="float:right;">
                                                        <div class="btn-group">

                                                            <a title="Add Sub menu" href="{{ route('admin.menu.plus.update', $row->id) }}"  class="btn btn-xs btn-success bootbox-confirm">
                                                                <i class="icon-angle-up bigger-120"></i>
                                                            </a>

                                                        </div>

                                                        {{--{{ $row->order_by }}--}}

                                                        <div class="btn-group">

                                                            <a class="btn btn-xs btn-danger " href="{{ route('admin.menu.minus.update', ['id' => $row->id]) }}">
                                                                <i class="icon-angle-down bigger-120"></i>
                                                            </a>

                                                        </div>
                                                    </div>
                                                </td>

                                                <td>
                                                    <style>
                                                        .danger{
                                                            color:#D15B47;
                                                        }
                                                    </style>

                                                    @if(isset($row->children) && count($row->children))
                                                        <a class="btn btn-xs btn-info" href="{{ route('admin.sub-menu.index',$row->slug) }}">Sub menu</a>
                                                        {{--@elseif(!isset($row->page))
                                                        <p class="danger">No Link</p>--}}
                                                        @else
                                                        <div class="btn-group">
                                                            <button class="btn btn-sm btn-yellow">{{ ($row->page)?$row->page->title:'No Link' }}</button>

                                                            <button data-toggle="dropdown" class="btn btn-sm btn-yellow dropdown-toggle" aria-expanded="false">
                                                                <i class="arrow icon-angle-down"></i>
                                                            </button>

                                                            <ul class="dropdown-menu dropdown-yellow">

                                                                <li>
                                                                    <a href="{{ route('admin.menu.store.page',['id' => $row->id,'value'=>0]) }}">No link</a>
                                                                </li>
                                                                @if(isset($data['page']) && count($data['page'])>0)
                                                                @foreach($data['page'] as $page)
                                                                    <li>
                                                                        <a href="{{ route('admin.menu.store.page',['id' => $row->id,'value'=>$page->id]) }}">{{ $page->title }}</a>
                                                                    </li>
                                                                    {{--<option {{ ($data['row']->page_id==$page->id)?'selected':'' }} value="{{ $page->id }}"></option>--}}
                                                                @endforeach
                                                                    @endif
                                                            </ul>
                                                        </div>
                                                    @endif
                                                </td>

                                                <td>
                                                    {{ ($row->section_id==0)?'no section select':$row->section->title }}
                                                </td>

                                                <td>
                                                        @if ($row->status == 1)
                                                            <a class="btn btn-xs btn-success" href="{{ route('admin.menu.status.update',$row->id) }}">Active</a>
                                                        @else
                                                            <a class="btn btn-xs btn-danger" href="{{ route('admin.menu.status.update',$row->id) }}">InActive</a>
                                                        @endif
                                                </td>



                                                @if(Auth::user()->type=="admin")
                                                    <td>

                                                        <div class="btn-group">

                                                            <a title="Add Sub menu" href="{{ route('admin.sub-menu.add', $row->slug) }}"  class="btn btn-xs btn-info bootbox-confirm">
                                                                <i class="icon-plus bigger-120"></i>
                                                            </a>

                                                        </div>

                                                        <div class="btn-group">

                                                            <a class="btn btn-xs btn-warning" href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                                <i class="icon-edit bigger-120"></i>
                                                            </a>

                                                        </div>

                                                        <div class="btn-group">


                                                            @if($row->slug == 'home'||$row->slug == 'gallery'||$row->slug == 'about-us'||$row->slug == 'information'||$row->slug == 'club-and-eca'||$row->slug == 'contact-us')
                                                            @else
                                                                <a href="{{ route($base_route.'.delete', $row->id) }}"  class="btn btn-xs btn-danger bootbox-confirm">
                                                                    <i class="icon-trash bigger-120"></i>
                                                                </a>
                                                            @endif

                                                        </div>

                                                        <div class="btn-group">
                                                            @if($row->target==1)
                                                            <a href="{{ route('admin.menu.target.update',$row->id) }}"  class="btn btn-xs btn-success bootbox-confirm">
                                                                Open in new tab
                                                            </a>
                                                            @else
                                                            <a href="{{ route('admin.menu.target.update',$row->id) }}"  class="btn btn-xs btn-info bootbox-confirm">
                                                                Open in current tab
                                                            </a>
                                                                @endif

                                                        </div>


                                                    </td>
                                                @endif
                                            </tr>

                                        @endforeach

                                    @else

                                        <tr>
                                            <td colspan="7">No data found.</td>
                                        </tr>

                                    @endif

                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-4">

                            </div>

                            <!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection

@section('page_specific_scripts')

    <script>
        $.ajax({
            type : 'post',
            url : ''
        });
    </script>

@endsection