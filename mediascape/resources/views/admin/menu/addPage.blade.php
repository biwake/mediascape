@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - {{ trans('general.admin_panel') }} @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.index') }}">{{ $view_title }} </a>
                </li>

                @if (isset($data))
                    <li class="active">Edit </li>
                @else
                    <li class="active">Add </li>
                @endif

            </ul><!-- .breadcrumb -->

        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $view_title }}<small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'content.add.add_form') }}</small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    {{--@if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif--}}


                    <form id="listing-form" class="form-horizontal" role="form" method="POST" action="{{ route('admin.menu.store.page',$id) }}" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        @if(isset($data['page']))
                        <div class="form-group">
                            <label class="col-sm-3 control-label no-padding-right" for="status"> Link With Page </label>

                            <div class="col-sm-9">
                                <select name="page_id" id="">
                                    <option value="0">No Link</option>
                                    @foreach($data['page'] as $page)
                                        <option {{ ($data['row']->page_id==$page->id)?'selected':'' }} value="{{ $page->id }}">{{ $page->title }}</option>
                                    @endforeach
                                </select>
                                {{--<div class="radio">
                                    <label>
                                        <input name="status" {{ (isset($data['row'])&&$data['row']->status==0)?'checked':'' }} value="0" type="radio" class="ace">
                                        <span class="lbl"> In-active</span>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input name="status" {{ (isset($data['row'])&&$data['row']->status==0)?'checked':'' }} value="2" type="radio" class="ace">
                                        <span class="lbl"> In-active</span>
                                    </label>
                                </div>--}}
                            </div>
                        </div>
                        <div class="space-4"></div>
                        @endif

                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button id="form-submit-btn" class="btn btn-info" type="submit">
                                    <i class="icon-ok bigger-110"></i>
                                    {{ trans('general.button.submit') }}
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="icon-undo bigger-110"></i>
                                    {{ trans('general.button.reset') }}
                                </button>
                            </div>
                        </div>

                        <div class="hr hr-24"></div>



                    </form>


                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection

@section('page_specific_scripts')

    @include($view_path.'.partials.add.jquery-validation-scripts')

@endsection