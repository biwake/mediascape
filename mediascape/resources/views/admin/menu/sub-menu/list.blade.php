@extends('admin.common.layout.layout')

@section('page_title'){{ $view_title }} - {{ trans('general.admin_panel') }} @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">Home</a>
                </li>

                <li>
                    <a href="{{ route($base_route.'.index',$data['slug']) }}">{{ $view_title }} </a>
                </li>

                @if (isset($data))
                    <li class="active">{{ trans('admin/fmenu/list.top.edit') }}</li>
                @else
                    <li class="active">{{ trans('admin/fmenu/list.top.add') }}</li>
                @endif

            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">

                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $view_title }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans('admin/fmenu/list.top.list') }}
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif

                                    <a class="btn btn-sm btn-info bootbox-confirm" href="{{ route('admin.menu.index') }}">Back</a>
                                    <a title="Add Sub menu" href="{{ route('admin.sub-menu.add', $slug) }}"  class="btn btn-sm btn-success bootbox-confirm">
                                        <i class="icon-plus bigger-120"></i> Add
                                    </a>
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            sn
                                        </th>
                                        <th>Name</th>
                                        <th>Page link</th>
                                        <th>Section Manage</th>
                                        <th>status</th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if ($data['sub-menu']->count() > 0)
                                        @php $counter=0; @endphp
                                        @foreach($data['sub-menu'] as $row)
                                            @php $counter++ @endphp

                                            <tr>

                                                <td class="center">
                                                    {{ $counter }}
                                                </td>

                                                <td>
                                                    {{ $row->title }}
                                                </td>

                                                <td>

                                                    @if(isset($row->children) && count($row->children))
                                                        <a class="btn btn-xs btn-info" href="{{ route('admin.sub-menu.index',$row->slug) }}">Sub menu</a>
                                                        @else
                                                        <div class="btn-group">
                                                            <button class="btn btn-sm btn-yellow">{{ \App\HelperClasses\ViewHelper::getPageValue($row) /* (isset($row->page))?$row->page->title:($row->page_id == 11111)?$row->page_id:'No link'*/ }}</button>

                                                            <button data-toggle="dropdown" class="btn btn-sm btn-yellow dropdown-toggle" aria-expanded="false">
                                                                <i class="arrow icon-angle-down"></i>
                                                            </button>

                                                            <ul class="dropdown-menu dropdown-yellow">

                                                                <li>
                                                                    <a href="{{ route('admin.menu.store.page',['id' => $row->id,'value'=>0]) }}">No link</a>
                                                                </li>
                                                                <li>
                                                                    <a href="{{ route('admin.menu.store.page',['id' => $row->id,'value'=>11111]) }}">Own Page</a>
                                                                </li>
                                                                @if(isset($data['page']) && count($data['page'])>0)
                                                                @foreach($data['page'] as $page)
                                                                    <li>
                                                                        <a href="{{ route('admin.menu.store.page',['id' => $row->id,'value'=>$page->id]) }}">{{ $page->title }}</a>
                                                                    </li>
                                                                    {{--<option {{ ($data['row']->page_id==$page->id)?'selected':'' }} value="{{ $page->id }}"></option>--}}
                                                                @endforeach
                                                                    @endif
                                                            </ul>
                                                        </div>
                                                    @endif
                                                </td>

                                                <td>

                                                    <a href="{{ route('admin.menu.section',$row->id) }}" class="btn btn-xs btn-info">
                                                        <i class="icon-plus"></i>
                                                        Choose Section
                                                    </a>

                                                </td>

                                                <td>
                                                    @if ($row->status == 1)
                                                        <a class="btn btn-xs btn-success" href="{{ route('admin.menu.status.update',$row->id) }}">Active</a>
                                                    @else
                                                        <a class="btn btn-xs btn-danger" href="{{ route('admin.menu.status.update',$row->id) }}">InActive</a>
                                                    @endif
                                                </td>



                                                @if(Auth::user()->type=="admin")
                                                    <td>

                                                        <div class="btn-group">

                                                            <a class="btn btn-xs btn-warning" href="{{ route($base_route.'.edit',  ['id' => $row->id]) }}">
                                                                <i class="icon-edit bigger-120"></i>
                                                            </a>

                                                        </div>

                                                        <div class="btn-group">

                                                            <a href="{{ route($base_route.'.delete', ['id' => $row->id]) }}"  class="btn btn-xs btn-danger bootbox-confirm">
                                                                <i class="icon-trash bigger-120"></i>
                                                            </a>

                                                        </div>

                                                        <div class="btn-group">
                                                            @if($row->target==0)
                                                            <a href="{{ route('admin.menu.target.update',$row->id) }}"  class="btn btn-xs btn-success bootbox-confirm">
                                                                Open in new tab
                                                            </a>
                                                            @else
                                                            <a href="{{ route('admin.menu.target.update',$row->id) }}"  class="btn btn-xs btn-info bootbox-confirm">
                                                                Open in current tab
                                                            </a>
                                                                @endif

                                                        </div>


                                                    </td>
                                                @endif
                                            </tr>

                                        @endforeach

                                    @else

                                        <tr>
                                            <td colspan="7">No data found.</td>
                                        </tr>

                                    @endif

                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-4">

                            </div>

                            <!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection

@section('page_specific_scripts')

    <script>
        $.ajax({
            type : 'post',
            url : ''
        });
    </script>

@endsection