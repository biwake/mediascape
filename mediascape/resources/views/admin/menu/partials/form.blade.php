<div class="space-4"></div>

<style>
    .error{
        color:darkred;
    }
</style>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="title"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="title" id="title" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
        @if (count($errors) > 0 && $errors->has('title'))
            <div class="clearfix"></div>
            <p class="error">{{ $errors->first('title') }}</p>
        @endif
    </div>
</div>
<div class="space-4"></div>

@if(!isset($data['row']))
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="slug"> Slug </label>

        <div class="col-sm-9">
            <input type="text" name="slug" id="slug" value="{{ ViewHelper::getData('slug', isset($data['row'])?$data['row']:[]) }}" placeholder="Slug" class="col-xs-10 col-sm-5">
            @if (count($errors) > 0 && $errors->has('slug'))
                <div class="clearfix"></div>
                <p class="error">{{ $errors->first('slug') }}</p>
            @endif
        </div>
    </div>
    <div class="space-4"></div>
@endif

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="section_id"> Section </label>

        <div class="col-sm-9">
            <select name="section_id" id="section_id">
                <option value="0">Select Section</option>
                @if(isset($data['section']) && count($data['section'])>0)
                    @foreach($data['section'] as $d)
                    <option {{ (isset($data['row']) && $d->id==$data['row']->section_id)?'selected':'' }} value="{{ $d->id }}">{{ $d->title }}</option>
                    @endforeach
                    @endif
            </select>
            {{--@if (count($errors) > 0 && $errors->has('slug'))
                <div class="clearfix"></div>
                <p class="error">{{ $errors->first('slug') }}</p>
            @endif--}}
        </div>
    </div>
    <div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="status"> Status </label>

    <div class="col-sm-9">
        <div class="radio">
            <label>
                <input name="status" value="1" {{ (isset($data['row'])&&$data['row']->status==1)?'checked':'' }} {{ isset($data['row'])?'':'checked' }} type="radio" class="ace">
                <span class="lbl"> Active</span>
            </label>
        </div>
        <div class="radio">
            <label>
                <input name="status" {{ (isset($data['row'])&&$data['row']->status==0)?'checked':'' }} value="0" type="radio" class="ace">
                <span class="lbl"> In-active</span>
            </label>
        </div>
    </div>
</div>
<div class="space-4"></div>


<script>

    $(document).ready(function () {

        $('#title').keyup(function () {
            var title = $('#title').val();
            $('#slug').val(title.replace(/\s+/g, '-').toLowerCase());
        });

        $('#select_parent_menu_yes').click(function(){
            $('#parent_menu_hide_gar').show(100);
        });

        $('#select_parent_menu_no').click(function(){
            $('#parent_menu_hide_gar').hide(100);
        });

    });

</script>