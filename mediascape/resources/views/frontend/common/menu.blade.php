<style>
    .serve:hover{
        color: #C4262C!important;
    }
    .serve{
        color: #ffffff!important;
    }

    /* The container <div> - needed to position the dropdown content */
    .dropdown {
        position: relative;
        display: inline-block;
    }

    /* Dropdown Content (Hidden by Default) */
    .dropdown-content {
        display: none;
        position: absolute;
        z-index: 1;
    }

    /* Links inside the dropdown */
    .dropdown-content a {
        color: black;
        text-decoration: none;
        display: block;
    }

    /* Show the dropdown menu on hover */
    .dropdown:hover .dropdown-content {
        display: block;
    }
    .mainmenu{
        color: #fff!important;
    }
    .activese a{
        color: #5F5E5C!important;
    }
</style>

<div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav col-md-11" style="margin-left: 10%;">

        <li class="{!! Request::is('/')?'activese':'' !!}"><a class="dropbtn mainmenu" href="{{ route('index') }}">Home</a></li>

        @foreach($menus as $menu)
            <li class="{{ $menu->children->count()>0?'dropdown mainmenu':'' }} {!! Request::is($menu->slug.'*')?'activese':'' !!}">
                <a class="dropbtn mainmenu" {{ ($menu->target==1)?'target="_blank"':'' }} href="{{ (isset($menu->children) && count($menu->children)>0)?'#':($menu->page_id==0)?url($menu->slug):route($menu->page->page_type,$menu->page->slug)  }}" >{{ $menu->title }}</a>
                @if(isset($menu->children) && count($menu->children)>0)
                    <ul class="dropdown-content" style="background-color: #C4262C">
                        @foreach($menu->children as $children)
                            @if($children->status ==0)
                                @continue
                            @endif
                            @php($child_page_type = ViewHelper::getsubMenu($children->page_id,isset($children->page->slug)?$children->slug:''))
                            <li style="list-style: none;">
                                <a class="mainmenu" href="{{ ($children->page_id==0 || $children->page_id==11111)?($children->page_id == 11111)?url($menu->slug.'/'.$children->slug):'#':route(isset($children->page->page_type)?$children->page->page_type.'s':$children->page->page_type.'s',['parent'=>$menu->slug,'child'=>$child_page_type]) }}">{{ $children->title }}</a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach

{{--
        @if(isset($section_menu) && count($section_menu)>0)

        <style>
            body{
                padding:10px;
            }

            .show-on-hover:hover > ul.dropdown-menu {
                display: block;
            }
            
            .extra_menu{
                background-color: #BA0F22;
                font-weight: bold;
                font-size: 18px;
            }
        </style>

        <div class="container">
            <div class="row">
                <div class="btn-group show-on-hover" style="margin-top: 5px;">
                    <a href="#" style="color: #fff;padding: 0px 20px;" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-align-justify"></i>
                    </a>
                    <ul class="dropdown-menu" style="margin-top: 0px!important;background-color: #BA0F22; " role="menu">
                        @foreach($section_menu as $menu)
                        <li><a {{ ($menu->target==1)?'target="_blank"':'' }} href="{{ (isset($menu->children) && count($menu->children)>0)?'#':($menu->page_id==0)?url($menu->slug):route($menu->page->page_type,$menu->page->slug)  }}" class="extra_menu">{{ $menu->title }}</a></li>
                            @endforeach
                    </ul>
                </div>
            </div>
        </div>

            @endif--}}

    </ul>
</div><!--/.nav-collapse -->


<script>
    $(function(){
        $('#menu_li').hover(
            function() {
                $('#menu_ul').fadeIn();
            },
            function() {
                $('#menu_ul').fadeOut();
            });
    });

</script>