<style>
    .activeses a{
        color: #C4262C!important;
    }
</style>

<div class="container demo">


    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        @php($counter = 0)
        @foreach($data['sidebar'] as $s)
            @php($counter++)
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne" style="background-color: #5F5E5C;">
                    <h4 class="panel-title {!! Request::is('our-team/'.$s->page->slug.'*')?'activeses':'' !!}">
                        @php($child_page_type = ViewHelper::getsubMenu($s->page_id,isset($s->page->slug)?$s->slug:''))
                        <a style="color: #ffffff;" id="accordion_btn{{ $counter }}" class="dropdown_a"
                           href="{{ ($s->page_id==0 || $s->page_id==11111)?($s->page_id == 11111)?url($parent.'/'.$s->slug):'#':route(isset($s->page->page_type)?$s->page->page_type.'s':$s->page->page_type.'s',['parent'=>$parent,'child'=>$child_page_type]) }}"
                        >
                            {{ $s->title }}
                        </a>
                    </h4>
                </div>
            </div>
        @endforeach

    </div>


</div>

<script>
    @php($counter = 0)
 @foreach($data['sidebar'] as $s)
     @php($counter++)

     @if($s->parent_id!=0)
         @continue
     @endif
$(document).ready(function () {
        $('#plus{{ $counter }}').click(function () {
            $('#plus{{ $counter }}').hide();
            $('#minus{{ $counter }}').show();
        });
        $('#accordion_btn{{ $counter }}').click(function () {
            $('#plus{{ $counter }}').show();
            $('#minus{{ $counter }}').hide();
        });
    });
    @endforeach
</script>