@extends('frontend.common.layout')
@section('title') Our Team @endsection
@section('content')

    <div class="col-md-9" style="margin-top: 20px;margin-bottom: 40px;min-height: 500px;">
    @if(isset($data['rows']) && count($data['rows'])>0)
        @foreach($data['rows'] as $row)
            <div style="margin-bottom: 50px;">
                <div class="col-md-4" style="margin-top: 50px;padding-left: 5%;">
                    <img src="{{ asset('images/listing-menu/'.$row->image) }}" width="100%" alt="">
                </div>
                <div class="col-md-8">
                    <div style="color: #5F5E5C;">

                        <h1 class="col-md-12" style="padding-bottom: 10px;color: #5F5E5C;text-align: center;">{{ $row->title }}</h1>

                        <div class="col-md-12">
                            <div class="col-md-12">
                                <p style="text-align: justify!important;">
                                    {!! $row->description !!}
                                </p>
                            </div>
                            <div class="col-md-1"></div>
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
    @endforeach
    @endif
    </div>
        <div class="col-md-3" style="margin-top: 65px;">@include('frontend.common.lis_sidebar')</div>

    @endsection