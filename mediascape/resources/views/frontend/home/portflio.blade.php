@extends('frontend.common.layout')
@section('title') Portflio @endsection
@section('content')
<div style="min-height: 500px;">
    <div class="container">
        <div class="col-md-12">

            <h3 style="text-align: center;padding: 30px 0 20px 0;font-size: 35px;color: #5f5e5c;"></h3>
            <div class="tcch col-md-12">
                <div class="tcch">

                    <style>

                        .gallery-title
                        {
                            font-size: 36px;
                            color: #BA0F22;
                            text-align: center;
                            font-weight: 500;
                            margin-bottom: 70px;
                        }
                        .gallery-title:after {
                            content: "";
                            position: absolute;
                            width: 7.5%;
                            left: 46.5%;
                            height: 45px;
                            border-bottom: 1px solid #5e5e5e;
                        }
                        .filter-button
                        {
                            font-size: 18px;
                            border: 1px solid #BA0F22;
                            border-radius: 5px;
                            text-align: center;
                            color: #BA0F22;
                            margin-bottom: 30px;

                        }
                        .filter-button:hover
                        {
                            font-size: 18px;
                            border: 1px solid #BA0F22;
                            border-radius: 5px;
                            text-align: center;
                            color: #ffffff;
                            background-color: #BA0F22;

                        }
                        .btn-default:active .filter-button:active
                        {
                            background-color: #BA0F22;
                            color: white;
                        }

                        .port-image
                        {
                            width: 100%;
                        }

                        .gallery_product
                        {
                            margin-bottom: 30px;
                        }

                    </style>

                    <div class="container">
                        <div class="row">
                            <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h1 class="gallery-title">Gallery</h1>
                            </div>

                            <div align="center">
                                @if(isset($data['gallery']) && count($data['gallery'])>0)
                                @foreach($data['gallery'] as $gallery)
                                <button class="btn btn-default filter-button" data-filter="image{{ $gallery->id }}">{{ $gallery->title }}</button>
                                    @endforeach
                                    @endif

                            </div>
                            <br/>


                            @if(isset($data['photo']) && count($data['photo'])>0)
                                @foreach($data['photo'] as $photo)
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter image{{ $photo->gallery_id }}">
                                        <img src="{{ asset('images/photo/'.$photo->image) }}" style="width: 100%;height: 250px;" class="img-responsive">
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
{{--

                    <div class="container">
                        <div class="row">
                            <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h1 class="gallery-title">Gallery</h1>
                            </div>

                            <div align="center">
                                <button class="btn btn-default filter-button" data-filter="all">All</button>
                                <button class="btn btn-default filter-button" data-filter="hdpe">HDPE Pipes</button>
                                <button class="btn btn-default filter-button" data-filter="sprinkle">Sprinkle Pipes</button>
                                <button class="btn btn-default filter-button" data-filter="spray">Spray Nozzle</button>
                                <button class="btn btn-default filter-button" data-filter="irrigation">Irrigation Pipes</button>
                            </div>
                            <br/>



                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>

                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>

                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>

                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter irrigation">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>

                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter spray">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>

                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter irrigation">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>

                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter spray">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>

                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter irrigation">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>

                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter irrigation">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>

                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>

                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter spray">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>

                            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                <img src="http://fakeimg.pl/365x365/" class="img-responsive">
                            </div>
                        </div>
                    </div>
--}}

                    <script>
                        $(document).ready(function(){

                            $(".filter-button").click(function(){
                                var value = $(this).attr('data-filter');

                                if(value == "all")
                                {
                                    //$('.filter').removeClass('hidden');
                                    $('.filter').show('1000');
                                }
                                else
                                {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
                                    $(".filter").not('.'+value).hide('3000');
                                    $('.filter').filter('.'+value).show('3000');

                                }
                            });

                            if ($(".filter-button").removeClass("active")) {
                                $(this).removeClass("active");
                            }
                            $(this).addClass("active");

                        });
                    </script>

                    {{--@php($counter = 0)
                    @foreach($data['gallery']  as $gallery)
                        @php($counter++)
                        <div class="col-md-6" style="margin-top: 50px;">
                            <div style="">
                                <a --}}{{--href="#" --}}{{--data-toggle="modal" data-target="#myModal">
                                    <img src="{{ asset('images/gallery/'.$gallery->image) }}" style="width: 90%;margin-left: 5%;height: 350px;border: 5px #D4D4D4 solid;" alt="">
                                </a>
                            </div>
                        </div>
--}}{{--
                        <div class="modal fade" id="myModalhoasdf{{ $counter }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <img src="{{ asset('images/gallery/'.$gallery->image) }}" alt="">
                            </div>
                        </div>--}}{{--
                    @endforeach--}}

                </div>
                <!-- technology-top -->
            </div>
        </div>
    </div>
</div>
    @endsection