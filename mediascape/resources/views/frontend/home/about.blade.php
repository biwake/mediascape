@extends('frontend.common.layout')
@section('title') About @endsection
@section('content')
    <div class="col-md-12" style="margin-top: 20px;margin-bottom: 40px;min-height: 500px;">
        <div class="col-md-12">
            <div style="color: #5F5E5C;">
                <h1 style="padding-bottom: 10px;color: #5F5E5C;text-align: center;">About Us</h1>
                @php($counter = 0)
                @foreach($data['about'] as $about)
                    @php($counter++)
                    <div class="col-md-6" >
                        <div style="padding: 10px 20px;">
                            <a style="text-align: justify!important; color: #5F5E5C;">{!! $about->description !!}</a>
                        </div>
                    </div>
                    <div class="{{ ($counter%2==0)?'clearfix':'' }}"></div>
                    @endforeach
            </div>
        </div>
    </div>


    @endsection