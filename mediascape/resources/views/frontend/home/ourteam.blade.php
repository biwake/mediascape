@extends('frontend.common.layout')
@section('title') Our Team @endsection
@section('content')
    <div class="col-md-12" style="margin-top: 20px;margin-bottom: 40px;min-height: 500px;">
        <div class="col-md-4" style="margin-top: 50px;padding-left: 5%">@include('frontend.common.sidebar1')</div>
        <div class="col-md-8">
            <div style="color: #5F5E5C;">

                <h1 class="col-md-12" style="padding-bottom: 10px;color: #5F5E5C;text-align: center;">{{ $data['ourteam']->title }}</h1>

                <div class="col-md-12">
                    <div class="col-md-11">
                        <p style="text-align: justify!important;">
                            {!! $data['ourteam']->description !!}
                        </p>
                    </div>
                    <div class="col-md-1"></div>
                </div>

            </div>
        </div>
    </div>


    @endsection