<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('',                    ['as' => 'index',             'uses' => 'WebPage\WebPageController@index']);
/*Route::get('',                    ['as' => 'frontend.home',             'uses' => 'WebPage\WebPageController@index']);*/
Route::get('home',function (){
    return redirect('admin/dashboard');
})->name('frontend.home');
Route::get('about-us',                    ['as' => 'frontend.about-us',             'uses' => 'WebPage\WebPageController@about']);
Route::get('contact-us',                    ['as' => 'frontend.contact-us',             'uses' => 'WebPage\WebPageController@contact']);
Route::get('portfolio',                    ['as' => 'frontend.portfolio',             'uses' => 'WebPage\WebPageController@portflio']);
Route::post('onlineorder',                    ['as' => 'frontend.onlineorder',             'uses' => 'WebPage\WebPageController@onlineorder']);
Route::get('services/',                    ['as' => 'frontend.services',             'uses' => 'WebPage\WebPageController@services']);
Route::get('services/{slug}',                    ['as' => 'frontend.service',             'uses' => 'WebPage\WebPageController@action']);
Route::get('our-team/{slug}',                    ['as' => 'frontend.ourteams',             'uses' => 'WebPage\WebPageController@ourteam']);
//Route::get('our-team',                    ['as' => 'frontend.our-team',             'uses' => 'WebPage\WebPageController@ourteam']);


Route::get('ip/{child}',                    ['as' => 'image_page',             'uses' => 'WebPage\WebPageController@imagePages']);
Route::get('sp/{child}',                    ['as' => 'simple_page',             'uses' => 'WebPage\WebPageController@simplePages']);
Route::get('slp/{child}',                    ['as' => 'simple_listing_page',             'uses' => 'WebPage\WebPageController@simpleListingPages']);
Route::get('ilp/{child}',                    ['as' => 'image_listing_page',             'uses' => 'WebPage\WebPageController@imageListingPages']);

Route::get('ip/{parent}/{child}',                    ['as' => 'image_pages',             'uses' => 'WebPage\WebPageController@imagePage']);
Route::get('sp/{parent}/{child}',                    ['as' => 'simple_pages',             'uses' => 'WebPage\WebPageController@simplePage']);
Route::get('slp/{parent}/{child}',                    ['as' => 'simple_listing_pages',             'uses' => 'WebPage\WebPageController@simpleListingPage']);
Route::get('ilp/{parent}/{child}',                    ['as' => 'image_listing_pages',             'uses' => 'WebPage\WebPageController@imageListingPage']);

Route::group(['prefix' => 'admin'],function (){
    Auth::routes();
});

/*Route::get('login',function (){
    return redirect()->route('admin/login');
});*/

Route::get('logout', [ 'as' => 'logout', 'uses' => 'Auth\LoginController@logout' ]);

//Route::get('/', function () {
//    return redirect('login');
//});

/*Route::get('/home', function () {
    return redirect()->route('admin.listing.index');
});*/

Route::group(['middleware' => ['auth', 'status'], 'prefix' => '', 'as' => 'admin.', 'namespace' => 'Admin\\'], function () {

    Route::get('admin/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::get('admin/error/{code}',                        ['as' => 'error',                 'uses' => 'DashboardController@error']);

    Route::get('admin/profile/setting', ['as' => 'profile.setting', 'uses' => 'ProfileController@index']);
    Route::post('admin/profile/updatePassword/{id}', ['as' => 'profile.updatePassword', 'uses' => 'ProfileController@updatePassword']);

    Route::get('admin/profile', ['as' => 'profile', 'uses' => 'ProfileController@edit']);
    Route::post('admin/profile/{id}/update', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);

    Route::get('admin/feedback',['as' => 'feedback.list', 'uses' => 'FeedbackController@index']);
    Route::get('admin/feedback/{id}/update',          ['as' => 'feedback.update',          'uses' => 'FeedbackController@update']);

    Route::get('admin/about-us',                        ['as' => 'about-us.index',           'uses' => 'AboutUsController@index']);
    Route::get('admin/about-us/add',                    ['as' => 'about-us.add',             'uses' => 'AboutUsController@add']);
    Route::post('admin/about-us/store',                 ['as' => 'about-us.store',           'uses' => 'AboutUsController@store']);
    Route::get('admin/about-us/{id}/edit',              ['as' => 'about-us.edit',            'uses' => 'AboutUsController@edit']);
    Route::post('admin/about-us/{id}/update',          ['as' => 'about-us.update',          'uses' => 'AboutUsController@update']);
    Route::get('admin/about-us/{id}/delete',           ['as' => 'about-us.delete',          'uses' => 'AboutUsController@delete']);

    Route::get('admin/gallery',                        ['as' => 'gallery.index',           'uses' => 'GalleryController@index']);
    Route::get('admin/gallery/add',                    ['as' => 'gallery.add',             'uses' => 'GalleryController@add']);
    Route::post('admin/gallery/store',                 ['as' => 'gallery.store',           'uses' => 'GalleryController@store']);
    Route::get('admin/gallery/{id}/edit',              ['as' => 'gallery.edit',            'uses' => 'GalleryController@edit']);
    Route::post('admin/gallery/{id}/update',          ['as' => 'gallery.update',          'uses' => 'GalleryController@update']);
    Route::get('admin/gallery/{id}/delete',           ['as' => 'gallery.delete',          'uses' => 'GalleryController@delete']);

    Route::get('admin/photo',                        ['as' => 'photo.index',           'uses' => 'PhotoController@index']);
    Route::get('admin/photo/add',                    ['as' => 'photo.add',             'uses' => 'PhotoController@add']);
    Route::post('admin/photo/store',                 ['as' => 'photo.store',           'uses' => 'PhotoController@store']);
    Route::get('admin/photo/{id}/edit',              ['as' => 'photo.edit',            'uses' => 'PhotoController@edit']);
    Route::post('admin/photo/{id}/update',          ['as' => 'photo.update',          'uses' => 'PhotoController@update']);
    Route::get('admin/photo/{id}/delete',           ['as' => 'photo.delete',          'uses' => 'PhotoController@delete']);

    Route::get('admin/banner',                        ['as' => 'banner.index',           'uses' => 'BannerController@index']);
    Route::get('admin/banner/add',                    ['as' => 'banner.add',             'uses' => 'BannerController@add']);
    Route::post('admin/banner/store',                 ['as' => 'banner.store',           'uses' => 'BannerController@store']);
    Route::get('admin/banner/{id}/edit',              ['as' => 'banner.edit',            'uses' => 'BannerController@edit']);
    Route::post('admin/banner/{id}/update',          ['as' => 'banner.update',          'uses' => 'BannerController@update']);
    Route::get('admin/banner/{id}/delete',           ['as' => 'banner.delete',          'uses' => 'BannerController@delete']);

    Route::get('admin/services',                        ['as' => 'services.index',           'uses' => 'ServicesController@index']);
    Route::get('admin/services/add',                    ['as' => 'services.add',             'uses' => 'ServicesController@add']);
    Route::post('admin/services/store',                 ['as' => 'services.store',           'uses' => 'ServicesController@store']);
    Route::get('admin/services/{id}/edit',              ['as' => 'services.edit',            'uses' => 'ServicesController@edit']);
    Route::post('admin/services/{id}/update',          ['as' => 'services.update',          'uses' => 'ServicesController@update']);
    Route::get('admin/services/{id}/delete',           ['as' => 'services.delete',          'uses' => 'ServicesController@delete']);

    Route::get('admin/contact',                        ['as' => 'contact.index',           'uses' => 'ContactController@index']);
    Route::get('admin/contact/add',                    ['as' => 'contact.add',             'uses' => 'ContactController@add']);
    Route::post('admin/contact/store',                 ['as' => 'contact.store',           'uses' => 'ContactController@store']);
    Route::get('admin/contact/{id}/edit',              ['as' => 'contact.edit',            'uses' => 'ContactController@edit']);
    Route::post('admin/contact/{id}/update',          ['as' => 'contact.update',          'uses' => 'ContactController@update']);
    Route::get('admin/contact/{id}/delete',           ['as' => 'contact.delete',          'uses' => 'ContactController@delete']);

    Route::get('admin/pakage',                        ['as' => 'pakage.index',           'uses' => 'PakageController@index']);
    Route::get('admin/pakage/add',                    ['as' => 'pakage.add',             'uses' => 'PakageController@add']);
    Route::post('admin/pakage/store',                 ['as' => 'pakage.store',           'uses' => 'PakageController@store']);
    Route::get('admin/pakage/{id}/edit',              ['as' => 'pakage.edit',            'uses' => 'PakageController@edit']);
    Route::post('admin/pakage/{id}/update',          ['as' => 'pakage.update',          'uses' => 'PakageController@update']);
    Route::get('admin/pakage/{id}/delete',           ['as' => 'pakage.delete',          'uses' => 'PakageController@delete']);

    Route::get('admin/onlineorder/{id}/reply',                    ['as' => 'onlineorder.reply',             'uses' => 'OnlineorderController@reply']);
    Route::post('admin/onlineorder/{id}/mail',                 ['as' => 'onlineorder.mail',           'uses' => 'OnlineorderController@mail']);
    Route::get('admin/onlineorder/{id}/edit',              ['as' => 'onlineorder.edit',            'uses' => 'OnlineorderController@edit']);
    Route::get('admin/onlineorder',                        ['as' => 'onlineorder.index',           'uses' => 'OnlineorderController@index']);
    Route::get('admin/onlineorder/{id}/update', ['as' => 'onlineorder.update', 'uses' => 'OnlineorderController@update']);
    Route::get('admin/onlineorder/{id}/delete',           ['as' => 'onlineorder.delete',          'uses' => 'OnlineorderController@delete']);

    Route::get('admin/menu',                        ['as' => 'menu.index',           'uses' => 'MenuController@index']);
    Route::get('admin/menu/add',               ['as' => 'menu.add',             'uses' => 'MenuController@add']);
    //Route::get('menuadmin./add/page/{id}',                    ['as' => 'menu.add.page',             'uses' => 'MenuController@addPage']);
    Route::get('admin/menu/store/{value}/page/{id}',                    ['as' => 'menu.store.page',             'uses' => 'MenuController@storePage']);
    Route::post('admin/menu/store',                 ['as' => 'menu.store',           'uses' => 'MenuController@store']);
    Route::get('admin/menu/{id}/edit',              ['as' => 'menu.edit',            'uses' => 'MenuController@edit']);
    Route::post('admin/menu/{id}/update',           ['as' => 'menu.update',          'uses' => 'MenuController@update']);
    Route::get('admin/menu/status/{id}/update',           ['as' => 'menu.status.update',          'uses' => 'MenuController@updateStatus']);
    Route::get('admin/menu/target/{id}/update',           ['as' => 'menu.target.update',          'uses' => 'MenuController@updateTarget']);
    Route::get('admin/menu/{id}/delete',            ['as' => 'menu.delete',          'uses' => 'MenuController@delete']);
    Route::get('admin/menu/{id}/santosh',            ['as' => 'menu.linkwithpage',          'uses' => 'MenuController@delete']);
    Route::get('menu/plus/{id}/update',           ['as' => 'menu.plus.update',          'uses' => 'MenuController@updatePlus']);
    Route::get('menu/minus/{id}/update',           ['as' => 'menu.minus.update',          'uses' => 'MenuController@updateMinus']);
    Route::get('menu/{id}/add/section',                        ['as' => 'menu.section',           'uses' => 'MenuController@section']);
    Route::post('menu/{id}/section/store',                        ['as' => 'menu.store.section',           'uses' => 'MenuController@storeSection']);
    Route::get('menu/{id}/edit/section',                        ['as' => 'menu.edit.section',           'uses' => 'MenuController@editSection']);

    Route::get('admin/page/{page_type}/list',                        ['as' => 'page.index',           'uses' => 'PageController@index']);
    Route::get('admin/page/demo',                    ['as' => 'page.demo',             'uses' => 'PageController@demo']);
    Route::get('admin/page/{page_type}/add',                        ['as' => 'page.add',           'uses' => 'PageController@add']);
    Route::post('admin/page/{page_type}/store',                 ['as' => 'page.store',           'uses' => 'PageController@store']);
    Route::get('admin/page/{page_type}/{id}/edit',              ['as' => 'page.edit',            'uses' => 'PageController@edit']);
    Route::post('admin/page/{page_type}/{id}/update',           ['as' => 'page.update',          'uses' => 'PageController@update']);
    Route::get('admin/page/{id}/status/update',           ['as' => 'page.status.update',          'uses' => 'PageController@updateStatus']);
    Route::get('admin/page/{page_type}/{id}/delete',            ['as' => 'page.delete',          'uses' => 'PageController@delete']);

    Route::get('admin/menu/{parent_slug}/sub-menu',                        ['as' => 'sub-menu.index',           'uses' => 'SubMenuController@index']);
    Route::get('admin/menu/{parent_slug}/sub-menu/add',                    ['as' => 'sub-menu.add',             'uses' => 'SubMenuController@add']);
    Route::post('admin/{id}/sub-menu/store',                 ['as' => 'sub-menu.store',           'uses' => 'SubMenuController@store']);
    Route::get('admin/menu/sub-menu/{id}/edit',              ['as' => 'sub-menu.edit',            'uses' => 'SubMenuController@edit']);
    Route::post('admin/sub-menu/{id}/update',           ['as' => 'sub-menu.update',          'uses' => 'SubMenuController@update']);
    Route::get('admin/sub-menu/{id}/delete',            ['as' => 'sub-menu.delete',          'uses' => 'SubMenuController@deletesasdfasdf']);
    Route::get('admin/sub-menu/{id}/delete',            ['as' => 'sub-menu.delete',          'uses' => 'SubMenuController@delete']);
    //Route::get('{parent_slugadmin.}/sub-menu/{id}/deletes',          ['as' => 'sub-menu.deletes',          'uses' => 'SubMenuController@delete']  );

    Route::get('admin/admin-menu',                        ['as' => 'admin-menu.index',           'uses' => 'AdminMenuController@index']);
    Route::get('admin/admin-menu/add',                    ['as' => 'admin-menu.add',             'uses' => 'AdminMenuController@add']);
    Route::post('admin/admin-menu/store',                 ['as' => 'admin-menu.store',           'uses' => 'AdminMenuController@store']);
    Route::get('admin/admin-menu/{id}/edit',              ['as' => 'admin-menu.edit',            'uses' => 'AdminMenuController@edit']);
    Route::post('admin/admin-menu/{id}/update',           ['as' => 'admin-menu.update',          'uses' => 'AdminMenuController@update']);
    Route::get('admin/admin-menu/{id}/delete',            ['as' => 'admin-menu.delete',          'uses' => 'AdminMenuController@delete']);

    Route::get('admin/listing-menu/{slug}',                        ['as' => 'listing-menu.index',           'uses' => 'ListingMenuController@index']);
    Route::get('admin/listing-menu/add/{slug}',                    ['as' => 'listing-menu.add',             'uses' => 'ListingMenuController@add']);
    Route::post('admin/listing-menu/store/{slug}',                 ['as' => 'listing-menu.store',           'uses' => 'ListingMenuController@store']);
    Route::get('admin/listing-menu/{id}/edit/{slug}',              ['as' => 'listing-menu.edit',            'uses' => 'ListingMenuController@edit']);
    Route::get('admin/listing-menu/edit/{val}',              ['as' => 'listing-menu.status.edit',            'uses' => 'ListingMenuController@editStatus']);
    Route::post('admin/listing-menu/{id}/update/{slug}',           ['as' => 'listing-menu.update',          'uses' => 'ListingMenuController@update']);
    Route::get('admin/listing-menu/{id}/delete/{slug}',            ['as' => 'listing-menu.delete',          'uses' => 'ListingMenuController@delete']);

    Route::get('admin/section',                        ['as' => 'section.index',           'uses' => 'SectionController@index']);
    Route::get('admin/section/add',                    ['as' => 'section.add',             'uses' => 'SectionController@add']);
    Route::post('admin/section/store',                 ['as' => 'section.store',           'uses' => 'SectionController@store']);
    Route::get('admin/section/{id}/edit',              ['as' => 'section.edit',            'uses' => 'SectionController@edit']);
    Route::post('admin/section/{id}/update',          ['as' => 'section.update',          'uses' => 'SectionController@update']);
    Route::get('admin/section/{id}/delete',           ['as' => 'section.delete',          'uses' => 'SectionController@delete']);

});


Route::get('{error}',function (){
    return redirect()->back();
});