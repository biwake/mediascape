<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
            $table->string('email', 171)->unique();
            $table->string('name', 100)->nullable();
            $table->string('address', 255)->nullable();
            $table->string('contact', 255)->nullable();
            $table->mediumText('image')->nullable();
            $table->string('password');
            $table->boolean('status', 1)->default(0);
            $table->rememberToken();
            $table->string('type');
            $table->text('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
