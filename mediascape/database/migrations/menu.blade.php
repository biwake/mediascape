<div class="sidebar" id="sidebar">
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="icon-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="icon-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="icon-group"></i>
            </button>

            <button class="btn btn-danger">
                <i class="icon-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- #sidebar-shortcuts -->

    <ul class="nav nav-list">

        <li class="{{ Request::is('dashboard')?'active':'' }}">
            <a href="{{ route('admin.dashboard') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> {{ trans('admin/dashboard/general.dashboard') }} </span>
            </a>
        </li>

        <li class="{{ Request::is('profile')?'active':'' }}">
            <a href="{{ route('admin.profile') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Profile </span>
            </a>
        </li>

        <li {!! Request::is('admin/menu*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Menu </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('admin/menu')?'class="active"':"" !!}>
                    <a href="{{ route('admin.menu.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('admin/menu/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.menu.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li class="{{ Request::is('admin/page*')?'active':'' }}">
            <a href="{{ route('admin.page.demo') }}">
                <i class="icon-user"></i>
                page manage
            </a>
        </li>

        <li {!! Request::is('admin/listing-menu*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-list"></i>
                <span class="menu-text"> Listing Page Manage </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                @if(isset($menu) && count($menu)>0)

                    @foreach($menu as $m)
                        <li {!! Request::is('admin/listing-menu*'.$m->slug)?'class="active open"':"" !!}>
                            <a href="#" class="dropdown-toggle">
                                <i class="icon-double-angle-right"></i>
                                <span class="menu-text"> {{ ucfirst($m->title) }} </span>
                                <b class="arrow icon-angle-down"></b>
                            </a>
                            <ul class="submenu">
                                <li {!! Request::is('admin/listing-menu/'.$m->slug)?'class="active"':"" !!}>
                                    <a href="{{ route('admin.listing-menu.index',$m->slug) }}">
                                        <i class="icon-double-angle-right"></i>
                                        List
                                    </a>
                                </li>
                                <li {!! Request::is('admin/listing-menu/add/'.$m->slug)?'class="active open"':"" !!}>
                                    <a href="{{ route('admin.listing-menu.add',$m->slug) }}">
                                        <i class="icon-double-angle-right"></i>
                                        Add
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endforeach

                @endif
            </ul>
        </li>

        <li class="{{ Request::is('feedback')?'active':'' }}">
            <a href="{{ route('admin.feedback.list') }}">
                <i class="icon-double-angle-right"></i>
                Feedback
            </a>
        </li>

        <li class="{{ Request::is('onlineorder*')?'active':'' }}">
            <a href="{{ route('admin.onlineorder.index') }}">
                <i class="icon-coffee"></i>
                Online Order
            </a>
        </li>

        <li {!! Request::is('about-us*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> About Us </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('about-us')?'class="active"':"" !!}>
                    <a href="{{ route('admin.about-us.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('about-us/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.about-us.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('banner*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Banner </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('banner')?'class="active"':"" !!}>
                    <a href="{{ route('admin.banner.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('banner/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.banner.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('pakage*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Pakage </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('pakage')?'class="active"':"" !!}>
                    <a href="{{ route('admin.pakage.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('pakage/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.pakage.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('gallery*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-camera"></i>
                <span class="menu-text"> Gallery </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('gallery')?'class="active"':"" !!}>
                    <a href="{{ route('admin.gallery.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('gallery/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.gallery.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        <li {!! Request::is('services*')?'class="active open"':"" !!}>
            <a href="#" class="dropdown-toggle">
                <i class="icon-search"></i>
                <span class="menu-text"> Services </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li {!! Request::is('services')?'class="active"':"" !!}>
                    <a href="{{ route('admin.services.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>
                <li {!! Request::is('services/add')?'class="active open"':"" !!}>
                    <a href="{{ route('admin.services.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

    </ul><!-- /.nav-list -->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>