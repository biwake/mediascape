<?php

use Illuminate\Database\Seeder;

class ProfileSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profile')->insert([
            'title' => 'Media Scape',
            'image' => 'no'
        ]);
    }
}
