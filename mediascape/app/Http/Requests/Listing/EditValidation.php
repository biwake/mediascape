<?php

namespace App\Http\Requests\Listing;

use Illuminate\Foundation\Http\FormRequest;

class EditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'              =>  'required|max:100',
        ];
    }

    public function messages() {
        return [
            'title.required'                => 'Please, Add Name.',
            'title.min'                     => 'Please, Add min 8 characters.',
        ];
    }
}
