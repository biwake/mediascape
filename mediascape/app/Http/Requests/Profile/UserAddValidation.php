<?php

namespace App\Http\Requests\AboutUs;

use Illuminate\Foundation\Http\FormRequest;

class AddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              =>  'required|min:10',
            'address'              =>  'required|min:3',
            'contact'              =>  'required|min:7',
            'email'              =>  'required|min:7',
            'password'              =>  'required|min:7',
        ];
    }

    public function messages() {
        return [
            'name.required'                => 'Please, Add Name.',
            'name.min'                     => 'Please, Add min 10 characters.',
            'address.required'                => 'Please, Add Address.',
            'address.min'                     => 'Please, Add min 3 characters.',
            'contact.required'                => 'Please, Add Contact.',
            'contact.min'                     => 'Please, Add min 7 characters.',
            'email.required'                => 'Please, Add Email Address.',
            'email.min'                     => 'Please, Add min 7 characters.',
            'password.required'                => 'Please, Add Password.',
            'password.min'                     => 'Please, Add min 7 characters.',
        ];
    }
}
