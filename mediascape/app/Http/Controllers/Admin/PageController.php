<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Page\Add;
use App\Models\AdminMenu;
use App\Models\ListingMenu;
use App\Models\Menu;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends AdminBaseController
{

    public $base_route = 'admin.page';
    public $view_path = 'admin/page';
    public $view_title = 'Page Manager';
    public $trans_path = 'page';
    public $folder_name = 'page';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index($page_type)
    {

        $data = [];
        $data['page'] = Page::get();

        $data['rows'] = Page::select()->where('page_type',$page_type)->paginate(10);

        return view(parent::loadDefaultVars($this->view_path.'.list'),compact('data','page_type'));
    }

    public function demo()
    {

        $data = [];
        $data['page'] = Page::get();

        return view(parent::loadDefaultVars($this->view_path.'.demo'),compact('data'));
        
    }

    public function add($page_type)
    {
        return view(parent::loadDefaultVars($this->view_path.'.add'),compact('data','page_type'));
    }

    public function store(Add $request,$page_type)
    {

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();

            $image->move($this->folder_path,$file_name);

        }else{
            $file_name = null;
        }

        Page::create([
            'title' => $request->get('title'),
            'slug' => str_slug($request->get('title')),
            'description' => $request->get('description'),
            'short-description' => set($request->get('short-description'))?$request->get('short-description'):null,
            'image' => $file_name,
            'page_type' => $page_type,
            'status' => $request->get('status'),
        ]);

        if ($page_type=='simple_listing_page' || $page_type == 'image_listing_page'){
            AdminMenu::create([
                'title' => $request->get('title'),
                'slug' => str_slug($request->get('title')),
                'page_type' => $page_type,
                'status' => $request->get('status')
            ]);
        }
        return redirect()->route($this->base_route.'.index',$page_type);

    }

    public function edit($page_type,$id)
    {

        $data = [];
        $data['row'] = Page::find($id);

        return View(parent::loadDefaultVars($this->view_path.'.edit'),compact('data','page_type'));

    }

    public function updateStatus($id)
    {

        if (!$data = Page::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $statsu = ($data->status==1)?0:1;

        $data->update([
            'status' => $statsu
        ]);

        return redirect()->back();

    }

    public function update(Request $request,$page_type,$id)
    {

        if (!$data = Page::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $destinationPath = $this->folder_path;

            $image->move($destinationPath,$file_name);

            if($data->image ==! '')
                unlink($this->folder_path.$data->image);
        }else{
            if (hasKey('oldimg')){
                $file_name = $request->get('oldimg');
            }else{
                $file_name = null;
            }
        }

        if ($page_type=='simple_listing_page' || $page_type == 'image_listing_page'){

            $admin_slug = Page::where('id',$id)->pluck('slug')->first();
            $admin_id = AdminMenu::where('slug',$admin_slug)->pluck('id')->first();

            if (!$admin_data = AdminMenu::find($admin_id))
                return redirect()->route('admin.error', ['code' => '500']);

            $admin_data->update([
                'title' => $request->get('title'),
                'status' => $request->get('status')
            ]);
        }

        $data->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'short-description' => set($request->get('short-description'))?$request->get('short-description'):null,
            'image' => $file_name,
            'page_type' => $page_type,
            'status' => $request->get('status'),
        ]);

        return redirect()->route($this->base_route.'.index',$page_type);

    }

    public function delete($page_type,$id)
    {

        if (!$listing = page::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $page_id_menu = Menu::where('page_id',$id)->get();
        foreach ($page_id_menu as $p){
            $a = Menu::find($p->id);
            $a->update([
                'page_id' => 0
            ]);
        }

        if ($page_type == 'image_listing_page' || $page_type == 'simple_listing_page'){

            $listing_menu_id = ListingMenu::where('admin_menu_slug',$listing->slug)->get();

            $admin_slug = page::where('id',$id)->pluck('slug')->first();
            $admin_id = AdminMenu::where('slug',$admin_slug)->pluck('id')->first();

            if ($admin_data = AdminMenu::find($admin_id)){
                $admin_data->delete();
            }

            if (isset($listing_menu_id) && count($listing_menu_id)>0){
                foreach ($listing_menu_id as $list){
                    $list->delete();
                }
            }
        }
        $listing->delete();

        return redirect()->back();

    }

}
