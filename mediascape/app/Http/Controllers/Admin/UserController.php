<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Listing\ListingAddValidation;
use App\Http\Requests\Listing\ListingEditValidation;
use App\Http\Requests\Listing\UserAddValidation;
use App\Models\Listing;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class UserController extends AdminBaseController
{
    protected $base_route = 'admin.user';
    protected $view_path = 'admin.user';
    protected $view_title = 'User Manger';
    protected $folder_name = 'user';
    protected $message = 'User';
    protected $folder_path;

    public function __construct()
    {
        //parent::__construct();
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
        //dd($this->folder_path);
    }


    public function index(Request $request)
    {
        //dd('ok');
        $data = [];
        $data['rows'] = User::select('id','name','address','contact','email','image','slug','status','created_by','updated_by')
            ->orderBy('id','desc')
            ->paginate(15);


        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        //$data = [];
        //$data['category'] = ProductCategory::select('id', 'title')->orderBy('title', 'asc')->get();

        //dd($this->Session->read('Auth.User.id'));
        //dd(Auth::user()->id);
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(UserAddValidation $request)
    {

        if($request->hasFile('image')){
            parent::checkFolderExist();

            $image = $request->file('image');
            $image_name = rand(4747, 9999).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path,$image_name);
        }


       User::create([
           'name'               =>     $request->get('name'),
           'address'                =>     str_slug($request->get('address')),
           'contact'           =>     $request->get('contact'),
           'image' => isset($image_name)?$image_name:'',
           'email'           =>     $request->get('email'),
           'password'     =>     bcrypt($request->get('password')),
           'type' => $request->get('type'),
           'slug'                =>     str_slug($request->get('name')),
           'created_by' => Auth::user()->id,
           'status' => 1
        ]);

        $request->session()->flash('message',  $this->message.' added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        //dd($id);
        $data = [];

        if (!$data['listing'] = User::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(Request $request, $id)
    {
        if (!$listing = User::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        if($request->hasFile('image')){
            parent::checkFolderExist();

            $image = $request->file('image');
            $image_name = rand(4747, 9999).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path,$image_name);
        }

        $listing->update([
            'name'               =>     $request->get('name'),
            'address'                =>     str_slug($request->get('address')),
            'contact'           =>     $request->get('contact'),
            'image' => isset($image_name)?$image_name:'',
            'email'           =>     $request->get('email'),
            'password'     =>     bcrypt($request->get('password')),
            'type' => $request->get('type')
        ]);




        $request->session()->flash('message', 'Listing updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
       //dd('enter');
        if (!$listing = Listing::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        $request->session()->flash('message', 'Listing deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}