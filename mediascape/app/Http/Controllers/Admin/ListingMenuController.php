<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Listing\AddValidation;
use App\Http\Requests\Listing\EditValidation;
use App\Models\AdminMenu;
use App\Models\Careers;
use App\Models\ListingMenu;
use App\Models\Page;
use Illuminate\Http\Request;
use Image;


class ListingMenuController extends AdminBaseController
{
    protected $base_route = 'admin.listing-menu';
    protected $view_path = 'admin.listing-menu';
    protected $view_title;
    protected $folder_name = 'listing-menu';
    protected $trans_path = 'listing-menu';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }


    public function index(Request $request,$slug)
    {
        $data = [];
        $this->view_title = ucwords(str_replace('-', ' ',$slug));
        $data['rows'] = ListingMenu::select('id', 'slug','title', 'image', 'description','status')
            ->orderBy('id')
            ->where('admin_menu_slug',$slug)
            ->paginate(30);

        $listing_table_ko_data = ListingMenu::where('admin_menu_slug',$slug)->first();
        $parent_menu = Page::where('slug',$listing_table_ko_data['admin_menu_slug'])->first();
        if($parent_menu['page_type'] == 'image_listing_page'){
            $data['image']='image_listing_page';
        }

        $data['slug'] = $slug;
        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data','slug'));
    }

    public function add(Request $request,$slug)
    {
        $this->view_title = ucwords(str_replace('-', ' ',$slug));
        $image_page = AdminMenu::where('slug',$slug)->pluck('page_type')->first();
        if ($image_page == 'image_listing_page'){
            $data['image'] = 'image_listing_page';
        }
        $data['slug'] = $slug;
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddValidation $request,$slug)
    {
        $this->view_title = ucwords(str_replace('-', ' ',$slug));
        $menu = AdminMenu::select('slug')->where('slug',$slug)->first();

        if(isset($menu)){
            $menu_id = $menu->slug;
        }else{
            $menu_id = 'carrers';
        }

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $destinationPath = $this->folder_path;

            $image->move($this->folder_path,$file_name);

        }else{
            $file_name=null;
        }

       $list = ListingMenu::create([
           'title'               =>     $request->get('title'),
           'slug'               =>     str_slug($request->get('title')),
           'description'          =>     $request->get('description'),
           'image'          =>     $file_name,
           'admin_menu_slug'          =>     $menu_id,
           'status' => $request->get('status'),
        ]);

        $request->session()->flash('message', $this->view_title.' added successfully.');
        return redirect()->route($this->base_route.'.index',$slug);
    }

    public function edit($id,$slug)
    {

        $this->view_title = ucwords(str_replace('-', ' ',$slug));
        $data = [];

        if (!$data['santosh'] = ListingMenu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['row'] = ListingMenu::find($id);
        $data['slug'] = $slug;
        $parent_menu = Page::where('slug',$data['row']['admin_menu_slug'])->first();
        if($parent_menu['page_type'] == 'image_listing_page'){
            $data['image']='image_listing_page';
        }

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id, $slug)
    {

        if (!$listing = ListingMenu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);



        $this->view_title = ucwords(str_replace('-', ' ',$slug));
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $destinationPath = $this->folder_path;

            $image->move($destinationPath,$file_name);

            if($listing->image ==! '')
                unlink($this->folder_path.$listing->image);
        }else{
            $file_name = $request->get('oldimg');
        }


        $listing->update([
            'title'               =>     $request->get('title'),
            'image'                =>    $file_name,
            'description'          =>     $request->get('description'),
            'status' => $request->get('status'),
        ]);




        $request->session()->flash('message', $this->view_title.' updated successfully.');
        return redirect()->route($this->base_route.'.index',$slug);
    }

    public function editStatus($id)
    {

        if (!$data = ListingMenu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $statsu = ($data->status==1)?0:1;

        $data->update([
            'status' => $statsu
        ]);

        return redirect()->back();

    }

    public function delete(Request $request, $id, $slug)
    {
       //dd('enter');
        $this->view_title = ucwords(str_replace('-', ' ',$slug));
        if (!$listing = ListingMenu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        if($listing->image ==! '')
            unlink($this->folder_path.$listing->image);

        $listing->delete();
        $request->session()->flash('message', $this->view_title.' deleted successfully.');
        return redirect()->route($this->base_route.'.index',$slug);
    }

    public function ajaxdelete(Request $request)
    {

        dd($request->all());

    }



}