<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Menu\Add;
use App\Models\Menu;
use App\Models\Page;
use App\Models\Section;
use Illuminate\Http\Request;
use DB;

class MenuController extends AdminBaseController
{

    public $base_route = 'admin.menu';
    public $view_path = 'admin/menu';
    public $view_title = 'Menu Manager';
    public $trans_path = 'santosh';
    public $folder_name = 'menu';

    public function index()
    {
        $data = [];
        $data['menu'] = Menu::select()->where('parent_menu','=',0)->orderBy('order_by','ASC')->get();
        $a =$data['sub-menu'] = Menu::select()->where('parent_menu','!=',0)->get();
        $data['page'] = Page::select('title','id')->get();

        $data['fixed'] = ['home','gallery'];

        return view(parent::loadDefaultVars($this->view_path.'.list'),compact('data'));
    }

    public function add()
    {

        $data['section'] = Section::select('id','title')->get();
        return view(parent::loadDefaultVars($this->view_path.'.add'),compact('data'));
        
    }

    public function addPage($id)
    {

        $data['page'] = Page::select('title','id')->get();
        $data['row'] = Menu::select('page_id')->where('id',$id)->first();

        return view(parent::loadDefaultVars($this->view_path.'.addPage'),compact('data','id'));
    }

    public function storePage($value,$id)
    {

        if (!$data = menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $data->update([
            'page_id' => $value,
        ]);

        $a = Menu::where('id',$id)->pluck('parent_menu')->first();
        $slug = Menu::where('id',$a)->pluck('slug')->first();

        if (isset($slug))
            return redirect()->back();

        else
            return redirect()->back();

    }

    public function store(Add $request)
    {
        $max_order_by = Menu::select('order_by')->max('order_by');
        $order_by = $max_order_by + 1;

        Menu::create([
            'title' => $request->get('title'),
            'slug' => str_slug($request->get('slug')),
            'link' => '',
            'page_id' => 0,
            'parent_menu' => 0,
            'order_by' => $order_by,
            'section_id' => $request->get('section_id'),
            'target' => 0,
            'status' => $request->get('status'),
        ]);
        return redirect()->route($this->base_route.'.index');

    }

    public function edit($id)
    {
        if (!$datas = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $data = [];
        $data['section'] = Section::select('id','title')->get();
        $data['row'] = $datas;

        return View(parent::loadDefaultVars($this->view_path.'.edit'),compact('data'));

    }

    public function update(Request $request,$id)
    {

        if (!$data = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $data->update([
            'title' => $request->get('title'),
            'section_id' => $request->get('section_id'),
            'status' => $request->get('status'),
        ]);

        return redirect()->route($this->base_route.'.index');

    }

    public function updateStatus($id)
    {

        if (!$data = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $statsu = ($data->status==1)?0:1;

        $data->update([
            'status' => $statsu
        ]);

        return redirect()->back();

    }

    public function updatePlus($id)
    {
        if (!$data = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        if ($data->order_by == 0){
            return redirect()->back();
        }

        $new_order_by = $data->order_by -1;

        $next = Menu::where('order_by',$new_order_by)->where('parent_menu',0)->first();

        if (isset($next) && count($next)>0){
            $next->update([
                'order_by' => $data->order_by
            ]);
        }

        $data->update([
            'order_by' => $new_order_by
        ]);

        return redirect()->back();

    }

    public function updateMinus($id)
    {
        if (!$data = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $max_order_by = Menu::select('order_by')->max('order_by');

        if ($data->order_by == $max_order_by)
            return redirect()->back();

        $new_order_by = $data->order_by +1;

        $next = Menu::where('order_by',$new_order_by)->where('parent_menu',0)->first();

        if (isset($next) && count($next)>0){
            $next->update([
                'order_by' => $data->order_by
            ]);
        }

        $data->update([
            'order_by' => $new_order_by
        ]);

        return redirect()->back();
    }

    public function updateTarget($id)
    {
        if (!$data = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $statsu = ($data->target==1)?0:1;

        $data->update([
            'target' => $statsu
        ]);

        return redirect()->back();

    }

    public function delete($id)
    {

        if (!$data = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        if($data->slug == 'home'||$data->slug == 'gallery'||$data->slug == 'about-us'||$data->slug == 'contact-us')
            return redirect()->route('admin.error', ['code' => '500']);

        foreach ($data->children as $child){
            $childata = Menu::find($child->id);
            $childata->delete();
        }

        $data->delete();

        return redirect()->back();

    }

    public function section($id)
    {
        $menu = Menu::find($id);

        $title = ucwords($menu->title);

        $menu_sections = $menu->sections()->pluck('section_id')->toArray();

        $sections = Section::select('id','title')->orderBy('id','ASC')->get();
        return view(parent::loadDefaultVars($this->view_path.'.addSection'),compact('sections','id','menu_sections','title'));
    }

    public function storeSection(Request $request,$id)
    {
        $menu = Menu::find($id);

        $sections = $request->except('_token');

        $menu->sections()->sync($sections);

        return redirect()->back()->with('info','Sections set');

    }

}
