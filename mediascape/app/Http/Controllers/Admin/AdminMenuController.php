<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Models\AdminMenu;
use App\Models\ListingMenu;
use App\Models\Page;
use Illuminate\Http\Request;
use Image;


class AdminMenuController extends AdminBaseController
{
    protected $base_route = 'admin.admin-menu';
    protected $view_path = 'admin.admin-menu';
    protected $view_title = 'AdminMenu Manger';
    protected $folder_name = 'admin-menu';
    protected $trans_path = 'admin-menu';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }


    public function index(Request $request)
    {
            $data = [];
            $data['rows'] = AdminMenu::select('id','title', 'page_type', 'status')
                ->orderBy('id')
                ->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add()
    {
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(Request $request)
    {

        $list = AdminMenu::create([
            'title'               =>     $request->get('title'),
            'slug'               =>     str_slug($request->get('title')),
            'status' => $request->get('status'),
        ]);

        $request->session()->flash('message', 'AdminMenu added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit($id)
    {

        if (!$data = AdminMenu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $data = [];
        $data['row'] = AdminMenu::find($id);
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(Request $request, $id)
    {

        if (!$data = AdminMenu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

            $admin_slug = AdminMenu::where('id',$id)->pluck('slug')->first();
            $admin_id = Page::where('slug',$admin_slug)->pluck('id')->first();

            if (!$admin_data = Page::find($admin_id))
                return redirect()->route('admin.error', ['code' => '500']);

        $admin_data->update([
            'title' => $request->get('title'),
            'status' => $request->get('status'),
        ]);

        $data->update([
            'title' => $request->get('title'),
            'status' => $request->get('status')
        ]);

        $request->session()->flash('message', 'AdminMenu updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
        if (!$listing = AdminMenu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $listingmenuid = ListingMenu::where('admin_menu_slug',$listing->slug)->get();

        $admin_slug = AdminMenu::where('id',$id)->pluck('slug')->first();
        $admin_id = Page::where('slug',$admin_slug)->pluck('id')->first();

        if ($admin_data = Page::find($admin_id)){
            $admin_data->delete();
        }

        if (isset($listingmenuid) && count($listingmenuid)>0){
            foreach ($listingmenuid as $list){
                $list->delete();
            }
        }

        $listing->delete();
        $request->session()->flash('message', 'AdminMenu deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function ajaxdelete(Request $request)
    {

        dd($request->all());

    }



}