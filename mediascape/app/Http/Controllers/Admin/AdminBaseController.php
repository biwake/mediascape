<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\AdminMenu;
use App\Models\Profile;
use DB, File, View;

class AdminBaseController extends Controller
{

    protected function loadDefaultVars($path)
    {

        View::composer($path, function($view){

            $view->with('base_route', $this->base_route);
            $view->with('view_path', $this->view_path.'.');
            $view->with('view_title', $this->view_title);
            $view->with('trans_path', $this->trans_path);
            $view->with('profile', Profile::select()->first());
            $view->with('menu', AdminMenu::select()->get());

        });

        return $path;

    }

}