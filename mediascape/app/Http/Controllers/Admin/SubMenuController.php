<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Menu\Add;
use App\Http\Requests\Menu\Edit;
use App\Models\Menu;
use App\Models\Page;
use Illuminate\Http\Request;

class SubMenuController extends AdminBaseController
{

    public $base_route = 'admin.sub-menu';
    public $view_path = 'admin.menu.sub-menu';
    public $view_title;
    public $trans_path = 'santosh';
    public $folder_name = 'menu';

    public function index($slug)
    {
        $this->view_title = ucwords(str_replace('-',' ',$slug));

        $data = [];
        $data['slug'] = $slug;
        $id = Menu::where('slug',$slug)->pluck('id')->first();
        $data['sub-menu'] = Menu::select()->where('parent_menu','=',$id)->get();
        $data['page'] = Page::select('title','id')->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'),compact('data','slug'));
    }

    public function add($slug)
    {
        $this->view_title = ucwords(str_replace('-',' ',$slug));

        $data['id'] = Menu::where('slug',$slug)->pluck('id')->first();
        return view(parent::loadDefaultVars($this->view_path.'.add'),compact('data'));
        
    }

    public function store(Add $request,$id)
    {
        $slug = Menu::where('id',$id)->pluck('slug')->first();

        Menu::create([
            'title' => $request->get('title'),
            'slug' => str_slug($request->get('slug')),
            'link' => '',
            'page_id' => 0,
            'parent_menu' => $id,
            'target' => 0,
            'status' => $request->get('status'),
        ]);
        return redirect()->route($this->base_route.'.index',$slug);

    }

    public function edit($id)
    {

        if (!$menu = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $data = [];
        $data['row'] = Menu::find($id);

        $slug = Menu::where('id',$id)->pluck('slug')->first();
        $sub_id = Menu::where('id',$id)->pluck('parent_menu')->first();
        $data['slug'] = Menu::where('id',$sub_id)->pluck('slug')->first();

        $this->view_title = ucwords(str_replace('-',' ',$slug));

        return View(parent::loadDefaultVars($this->view_path.'.edit'),compact('data'));

    }

    public function update(Edit $request,$id)
    {

        if (!$data = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $data->update([
            'title' => $request->get('title'),
            'status' => $request->get('status'),
        ]);

        $sub_id = Menu::where('id',$id)->pluck('parent_menu')->first();
        $slug = Menu::where('id',$sub_id)->pluck('slug')->first();

        return redirect()->route($this->base_route.'.index',$slug);

    }

    public function delete($id)
    {

        if (!$data = Menu::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $data->delete();

        return redirect()->back()->with('success', 'Delete successful.');

    }

}
