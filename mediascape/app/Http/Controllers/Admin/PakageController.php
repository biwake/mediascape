<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Pakage\AddValidation;
use App\Http\Requests\Pakage\EditValidation;
use App\Models\Pakage;
use App\Models\Photo;
use Illuminate\Http\Request;


class PakageController extends AdminBaseController
{
    protected $base_route = 'admin.pakage';
    protected $view_path = 'admin.pakage';
    protected $view_title = 'Pakage Manger';
    protected $folder_name = 'pakage';
    protected $trans_path = '';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }


    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = Pakage::select('id','title', 'status')
            ->orderBy('id')
            ->paginate(30);

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddValidation $request)
    {

       $list = Pakage::create([
           'title'               =>     $request->get('title'),
           'status' => $request->get('status'),
        ]);

        $request->session()->flash('message', 'Pakage added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        $data = [];

        if (!$data['row'] = Pakage::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['row'] = Pakage::find($id);

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {
        if (!$listing = Pakage::find($id))
            return redirect()->route('admin.error', ['code' => '500']);


        $listing->update([
            'title'               =>     $request->get('title'),
            'status' => $request->get('status'),
        ]);

        $request->session()->flash('message', 'Pakage updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
       //dd('enter');
        if (!$listing = Pakage::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        $request->session()->flash('message', 'Pakage deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}