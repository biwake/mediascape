<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Models\Order;
use Illuminate\Http\Request;
use Auth;


class OnlineorderController extends AdminBaseController
{
    protected $base_route = 'admin.onlineorder';
    protected $view_path = 'admin.onlineorder';
    protected $view_title = 'Order Manger';
    protected $trans_path = 'Order Manger';


    public function index(Request $request)
    {

        $data = [];
        $data['rows'] = Order::select('id','username', 'email', 'pakage_id', 'message','status')
            ->orderBy('id')
            ->paginate(30);

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function reply($id)
    {

        $data = [];
        $data['id'] = $id;

        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
        
    }

    public function mail(Request $request, $id)
    {



    }

    public function update(Request $request, $id)
    {
        if (!$lis = Order::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $lis->update([
            'status' => ($lis['status']==0)?1:0,
        ]);

        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
       //dd('enter');
        if (!$listing = Order::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        $request->session()->flash('message', 'Order deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}