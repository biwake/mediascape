<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Photo\AddValidation;
use App\Models\Gallery;
use App\Models\Photo;
use Illuminate\Http\Request;


class PhotoController extends AdminBaseController
{
    protected $base_route = 'admin.photo';
    protected $view_path = 'admin.photo';
    protected $view_title = 'Photo Manger';
    protected $folder_name = 'photo';
    protected $trans_path = '';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }


    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = Photo::select('id','title', 'image', 'description', 'status')
            ->orderBy('id')
            ->paginate(30);
        $data['group'] = Gallery::select('id')->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {

        $data = [];
        $data['gallery'] = Gallery::select('id','title')->get();
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddValidation $request)
    {
        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }

       $list = Photo::create([
           'title'               =>     $request->get('title'),
           'image'                =>    $file_name,
           'gallery_id' => $request->get('gallery_id'),
           'description' => $request->get('description'),
           'status' => $request->get('status'),
        ]);

        $request->session()->flash('message', 'Photo added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        //dd($id);
        $data = [];

        $data['gallery'] = Gallery::select('id','title')->get();
        if (!$data['row'] = Photo::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['row'] = Photo::find($id);

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }else{
            $file_name = $request->get('oldimg');
        }

        if (!$listing = Photo::find($id))
            return redirect()->route('admin.error', ['code' => '500']);


        $listing->update([
            'title'               =>     $request->get('title'),
            'image'                =>    $file_name,
            'status' => $request->get('status'),
        ]);




        $request->session()->flash('message', 'Photo updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
       //dd('enter');
        if (!$listing = Photo::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $listing->delete();
        $request->session()->flash('message', 'Photo deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}