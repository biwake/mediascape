<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Gallery\AddValidation;
use App\Http\Requests\Gallery\EditValidation;
use App\Models\Gallery;
use App\Models\Page;
use App\Models\Photo;
use Illuminate\Http\Request;


class GalleryController extends AdminBaseController
{
    protected $base_route = 'admin.gallery';
    protected $view_path = 'admin.gallery';
    protected $view_title = 'Gallery Manger';
    protected $folder_name = 'gallery';
    protected $trans_path = '';
    protected $folder_path;

    public function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }


    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = Gallery::select('id','title', 'image', 'status')
            ->orderBy('id')
            ->paginate(30);

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddValidation $request)
    {

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }


       $list = Gallery::create([
           'title'               =>     $request->get('title'),
           'slug'               =>     str_slug($request->get('title')),
           'image'                =>    'test',
           'status' => 0,
        ]);

        $request->session()->flash('message', 'Gallery added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        //dd($id);
        $data = [];

        if (!$data['santosh'] = Gallery::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['row'] = Gallery::find($id);

        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(EditValidation $request, $id)
    {
        $photo = Photo::select('gallery_id')->where('gallery_id',$id)->get();

        if ($photo->count()>0){
            $status = 0;
        }else{
            $status = 1;
        }

        if ($request->hasFile('image')){
            $image = $request->file('image');
            $file_name = rand(0000 , 9999) . '_' . $image->getClientOriginalName();
            $image->move($this->folder_path,$file_name);
        }else{
            $file_name = $request->get('oldimg');
        }

        if (!$listing = Gallery::find($id))
            return redirect()->route('admin.error', ['code' => '500']);


        $listing->update([
            'title'               =>     $request->get('title'),
            'slug'               =>     str_slug($request->get('title')),
            'image'                =>    'test',
            'status' => $status,
        ]);




        $request->session()->flash('message', 'Gallery updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
       //dd('enter');
        if (!$gallery = Gallery::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $photo = Photo::where('gallery_id',$gallery->id)->get();

        /*delete photo which relited with this gallery*/
        if (isset($photo) && count($photo)>0){
            foreach ($photo as $p){
                $p->delete();
            }
        }

        // remove image before deleting db row

        $gallery->delete();
        $request->session()->flash('message', 'Gallery deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}