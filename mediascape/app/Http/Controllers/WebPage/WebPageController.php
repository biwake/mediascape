<?php

namespace App\Http\Controllers\WebPage;

use App\Models\ListingMenu;
use App\Models\Menu;
use App\Models\Order;
use App\Models\AboutUs;
use App\Models\Banner;
use App\Models\Feature;
use App\Models\Gallery;
use App\Models\News;
use App\Models\Page;
use App\Models\Pakage;
use App\Models\Photo;
use App\Models\Principle;
use App\Models\Profile;
use App\Models\Section;
use App\Models\Services;
use Illuminate\Http\Request;

class WebPageController extends BasicWebPageController
{

    protected $view_path = 'frontend.home.';

    public function index(){

        $data = [];
        $data['banner'] = Banner::select('id', 'title', 'image', 'description','status')
            ->orderBy('id')
            ->limit(4)
            ->where('status', 1)
            ->get();
        $data['profile'] = Profile::first();
        $data['principle'] = Principle::select('id','title', 'image', 'message','status')->first();
        $data['news'] = News::select('title', 'slug', 'image', 'short_description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->limit(3)
            ->get();
        $data['feature'] = Feature::select('title', 'slug', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->limit(5)
            ->get();
        $data['pakage'] = Pakage::select('id','title')->where('status',1)->get();
        $data['services'] = Services::select('title', 'id', 'image', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->get();
        $data['gallery'] = Photo::select('id', 'title', 'image', 'status')
            ->get();
        $data['about'] = AboutUs::select('id','title', "description", 'status')
            ->where('status', 1)
            ->first();

        return view(Parent::loadDefaultVars($this->view_path.'index'), compact('data'));

    }

    public function about()
    {
        $data['about'] = AboutUs::select('id','title', 'description', "image", 'status')
            ->where('status', 1)
            ->get();
        $data['profile'] = Profile::first();
        $data['services'] = Services::select('title', 'id', 'image', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->get();

        return view(Parent::loadDefaultVars('frontend.home.about'), compact('data'));
    }

    public function contact()
    {
        $data = [];
        $data['profile'] = Profile::first();
        $data['services'] = Services::select('title', 'id', 'image', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->get();
        return view(Parent::loadDefaultVars('frontend.home.contact'), compact('data'));
    }

    public function portflio()
    {
        $data['gallery'] = Gallery::select('id', 'title')
            ->get();
        $data['photo'] = Photo::select('id', 'title', 'gallery_id', 'image')
            ->get();
        $data['profile'] = Profile::first();
        $data['services'] = Services::select('title', 'id', 'image', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->get();
        return view(Parent::loadDefaultVars('frontend.home.portflio'), compact('data'));
    }

    public function services($slug=null)
    {
        $data = [];
        $data['profile'] = Profile::first();
        $data['services'] = Services::select('title', 'id', 'image', 'description','status')
            ->orderBy('id')
            ->where('status', 1)
            ->get();
        $data['service'] = Services::all()->first();
        return view(Parent::loadDefaultVars('frontend.home.services'), compact('data'));
    }

    public function onlineorder(Request $request)
    {
        $set = Order::create([
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'pakage_id' => $request->get('pakege'),
            'message' => $request->get('message'),
            'status' => 0,
        ]);

        $request->session()->flash('message', 'Online order successfully. Now check your email.');
        return redirect()->route('frontend.home');
    }

    public function ourteam($slug=null)
    {

        $a = isset($slug)?$slug:'creative-department';

        $data = [];
        $data['our-team'] = Menu::select()->where('slug','=',$a)->first();
        $parent_id = Menu::select('id')->where('slug','=','our-team')->first();
        $data['sidebar'] = Menu::select()->where('parent_menu','=',$parent_id->id)->get();

        return view(Parent::loadDefaultVars('frontend.home.ourteam'), compact('data'));
    }

    /*for extra pages*/

    public function page($parent,$child)
    {

        $page = Menu::where('slug',$child)->first();

        $data['m'] = Menu::select()->where('slug','=',$child)->first();
        $data['p'] = Menu::select()->where('id','=',$data['m']->parent_menu)->first();
        $data['sidebar'] = $data['p']->children;
        $data['page'] = $page->page;

        if(isset($data['page']['image']) && !empty($data['page']['image']))
            return view(Parent::loadDefaultVars('frontend.home.page1'), compact('data','parent','child'));

        else
            return view(Parent::loadDefaultVars('frontend.home.page'), compact('data','parent','child'));
    }

    /*for extra pages*/

    public function simplePage($parent,$child)
    {

        $title = ucwords(str_replace('-',' ',$child));

        $page = Menu::where('slug',$child)->first();

        $data['menu'] = Menu::select()->where('slug','=',$child)->first();


        $parent_id = Menu::select('id')->where('slug','=',$parent)->first();
        $data['sidebar'] = Menu::select()->where('parent_menu','=',$parent_id->id)->get();

        return view(Parent::loadDefaultVars('frontend.home.page.simple_page'), compact('data','parent','title'));

    }

    /*for extra pages*/

    public function simplePages($child)
    {

        $title = ucwords(str_replace('-',' ',$child));

        $page = Menu::where('slug',$child)->first();

        $data['menu'] = Menu::select()->where('slug','=',$child)->first();
        /*$data['p'] = Menu::select()->where('id','=',$data['m']->parent_menu)->first();
        $data['sidebar'] = $data['p']->children;
        $data['page'] = $page->page;*/

        return view(Parent::loadDefaultVars('frontend.home.page.simple_page'), compact('data','title'));

    }

    /*for extra pages*/

    public function imagePage($parent,$child)
    {
        $page = Menu::where('slug',$child)->first();

        $data['menu'] = Menu::select()->where('slug','=',$child)->first();
        /*$data['p'] = Menu::select()->where('id','=',$data['m']->parent_menu)->first();
        $data['sidebar'] = $data['p']->children;
        $data['page'] = $page->page;*/

        return view(Parent::loadDefaultVars('frontend.home.page.simple_image_page'), compact('data','child'));

    }

    /*for extra pages*/

    public function imagePages($child)
    {

        $page = Menu::where('slug',$child)->first();

        $data['m'] = Menu::select()->where('slug','=',$child)->first();
        $data['p'] = Menu::select()->where('id','=',$data['m']->parent_menu)->first();
        $data['sidebar'] = $data['p']->children;
        $data['page'] = $page->page;

        return view(Parent::loadDefaultVars('frontend.home.page.simple_image_page'), compact('data','child'));

    }

    /*for extra pages*/

    public function simpleListingPage($parent,$child)
    {

        $real_slug = $child;
        $data = [];

        $title = ucfirst($real_slug);
        $data['title'] =  str_replace("-"," ",$title);

        $page_id = menu::where('slug',$child)->pluck('page_id')->first();
        $page = Page::where('id',$page_id)->first();

        $data['listing_data'] = ListingMenu::select()->where('admin_menu_slug',$page->slug)->get();

        return view(Parent::loadDefaultVars('frontend.home.page.listing_page'), compact('data','child'));

    }

    /*for extra pages*/

    public function simpleListingPages($child)
    {

        $real_slug = $child;
        $data = [];

        $title = ucfirst($real_slug);
        $data['title'] =  str_replace("-"," ",$title);

        $page_id = menu::where('slug',$child)->pluck('page_id')->first();
        $page = Page::where('id',$page_id)->first();

        $data['listing_data'] = ListingMenu::select()->where('admin_menu_slug',$page->slug)->get();

        return view(Parent::loadDefaultVars('frontend.home.page.listing_page'), compact('data','child'));

    }

    /*for extra pages*/

    public function imageListingPage($parent,$child)
    {
        $real_slug = $child;
        $data = [];

        $title = ucfirst($real_slug);
        $data['title'] =  str_replace("-"," ",$title);

        $page_id = menu::where('slug',$child)->pluck('page_id')->first();
        $page = Page::where('id',$page_id)->first();

        $data['listing_data'] = ListingMenu::select()->where('admin_menu_slug',$page->slug)->get();

        return view(Parent::loadDefaultVars('frontend.home.page.listing_image_page'), compact('data','child'));

    }

    /*for extra pages*/

    public function imageListingPages($child)
    {
        $real_slug = $child;
        $data = [];

        $title = ucfirst($real_slug);
        $data['title'] =  str_replace("-"," ",$title);

        $page_id = menu::where('slug',$child)->pluck('page_id')->first();
        $page = Page::where('id',$page_id)->first();

        $data['rows'] = ListingMenu::select()->where('admin_menu_slug',$page->slug)->get();

        $section_menu_id = Section::select('id')->where('slug','=','section-nav-bar')->first();
        $data['sidebar'] = Menu::select()->where('section_id','=',$section_menu_id->id)->orderBy('order_by')->get();

        return view(Parent::loadDefaultVars('frontend.home.page.listing_image_page'), compact('data'));

    }

}
