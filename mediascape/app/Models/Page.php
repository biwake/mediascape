<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Page extends Model
{
    protected $table = 'page';

    protected $fillable = ['title', 'description', 'short-description', 'icon', 'image', 'page_type', 'slug', 'status'];

    public function menus()
    {
        return $this->hasMany('App\Models\Menu','page_id');
    }

}
