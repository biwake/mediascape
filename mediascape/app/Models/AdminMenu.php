<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AdminMenu extends Model
{
    protected $table = 'admin_menu';

    protected $fillable = ['title', 'slug', 'page_type', 'parent_menu', 'status'];

    public function listingMenu()
    {
        return $this->hasMany('App\Models\ListingMenu','admin_menu_slug');
    }


}
