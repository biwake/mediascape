<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Principle extends Model
{
    protected $table = 'principle';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['title', 'image', 'slug', 'message', 'status'];




}
