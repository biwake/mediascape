<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ListingMenu extends Model
{
    protected $table = 'listing_menu';

    protected $fillable = ['title', 'slug', 'description', 'image', 'admin_menu_slug', 'status'];

    public function adminMenu()
    {
        return $this->belongsTo('App\Models\AdminMenu','slug');
    }

}
