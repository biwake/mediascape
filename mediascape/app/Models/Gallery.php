<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['title', 'slug', 'image', 'status'];




}
