<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['title', 'slug', 'image','short_description', 'long_description', 'status'];




}
