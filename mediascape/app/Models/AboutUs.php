<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    protected $table = 'about_us';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['title', 'slug', 'image', 'icon', 'description', 'status'];




}
